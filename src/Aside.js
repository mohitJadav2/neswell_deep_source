import React, { useState, useEffect } from "react";
import { useIntl } from "react-intl";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";
import { FiLogOut, FiUsers, FiBox } from "react-icons/fi";
import { BsCardChecklist, BsBox } from "react-icons/bs";
import { AiOutlineExperiment } from "react-icons/ai";
import { GrCalculator } from "react-icons/gr";
import TopLogo from "./assets/images/sidebar-logo.svg";
import ProfileIcon from "./assets/images/user-profile-icon.svg";
import { Link, useHistory } from "react-router-dom";
import { saveSidebarName,changeFilterStatus } from "./Actions/HomeAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { API_URL } from "./config";
import DashboardApi from "./services/DashboardApi";
import LoginApi from "./services/LoginApi";

const Aside = ({
  collapsed,
  rtl,
  toggled,
  handleToggleSidebar,
  userDetails,
  saveSidebarName,
  sideBarName,
  adminData,
  changeFilterStatus
}) => {
  const intl = useIntl();
  let history = useHistory();

  /**
   *  This function manage the sidebar menu for setting the active tab variable 
   */
  const handelCurrentLink = async (linkName) => {
    await saveSidebarName(linkName);
  };

  const handleChangeFilterStatus = async () => {
    await changeFilterStatus(true)
  }

  /**
   * This fucntion manage logout and also remove the access token from the local storage 
   * And user can't move to the dashboard without login
   */
  const handleLogOutClicked = () => {
    DashboardApi.logOutApi(localStorage.getItem("neswell_access_token")).then(
      (json) => {
        if (!json?.data?.error) {
          localStorage.setItem("neswell_access_token", "null");
          history.push(`/`);
        }
      }
    );
  };

  /**
    This useEffect() manage user can't go to the dashboard without the login 
    */
  useEffect(() => {
    if (
      localStorage.getItem("neswell_access_token") === "null" ||
      !localStorage.getItem("neswell_access_token")
    ) {
    }
    LoginApi.varifyToken(localStorage.getItem("neswell_access_token")).then(
      (json) => {
        if (json?.data?.status) {
        } else {
          localStorage.setItem("neswell_access_token", "null");
          history.push(`/`);
        }
      }
    );
  }, []);

  /**
   * This function manage side bar as pre current page
   */
  useEffect(() => {
    if (
      window.location.href.includes("bio-test-view") ||
      window.location.href.includes("view-bio-details") ||
      window.location.href.includes("create-bio")
    ) {
      saveSidebarName("bioTestes");
    }
    else if( window.location.href.includes("sub-bio-test-list")){
      saveSidebarName("subBioTestes");
    }
    else if (
      window.location.href.includes("users-extract") ||
      window.location.href.includes("view-extract") ||
      window.location.href.includes("create-extract")
    ) {
      saveSidebarName("users-extract");
    }
    else if (
      window.location.href.includes("users") ||
      window.location.href.includes("users-profile") ||
      window.location.href.includes("add-users")
    ) {
      saveSidebarName("Users");
    } else if (
      window.location.href.includes("experiment-data")
    ) {
      saveSidebarName("experiment");
    } else if (
      window.location.href.includes("medical-indication")
    ) {
      saveSidebarName("MedicalIndication");
    } else if (
      window.location.href.includes("suppliers-data")
    ) {
      saveSidebarName("Suppliers");
    }
  });
  return (
    <ProSidebar
      rtl={rtl}
      collapsed={collapsed}
      toggled={toggled}
      breakPoint="md"
      onToggle={handleToggleSidebar}>
      <SidebarHeader>
        <div
          style={{
            padding: "15px 24px",
            textTransform: "uppercase",
            fontWeight: "bold",
            fontSize: 14,
            letterSpacing: "1px",
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            textAlign: "center",
            backgroundColor: "#1C6FE9",

          }}>
          <img style={{ maxWidth: 150, }} src={TopLogo} alt="" />
        </div>
      </SidebarHeader>

      <SidebarContent>
        <Menu iconShape="circle">

          {adminData?.permissions == "Admin" && (
            <MenuItem
              className={sideBarName === "Users" && "active"}
              onClick={() => handelCurrentLink("Users")}
              icon={<FiUsers />}>
              {" "}
              {intl.formatMessage({ id: "Users" })} <Link to="/users" />{" "}
            </MenuItem>
          )}
          {(adminData?.permissions == "Editor" ||
            adminData?.permissions == "Admin") && (
              <MenuItem
                className={sideBarName === "users-extract" && "active"}
                onClick={() => {
                  handelCurrentLink("users-extract")
                  handleChangeFilterStatus()
                }}
                icon={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24.449"
                    height="22.584"
                    viewBox="0 0 24.449 22.584"
                  >
                    <g
                      id="Layer_2"
                      data-name="Layer 2"
                      transform="translate(-1 -1.993)"
                    >
                      <path
                        id="Path_269"
                        data-name="Path 269"
                        d="M22.628,10.463a2.88,2.88,0,0,0-.385.039L19.987,6.74a2.814,2.814,0,1,0-4.7-2.857H11.171A2.814,2.814,0,1,0,6.464,6.737L4.208,10.5a2.88,2.88,0,0,0-.386-.036,2.821,2.821,0,0,0,0,5.642,2.88,2.88,0,0,0,.385-.039l2.257,3.761a2.814,2.814,0,1,0,4.708,2.86h4.107a2.814,2.814,0,1,0,4.7-2.857l2.257-3.761a2.88,2.88,0,0,0,.391.036,2.821,2.821,0,0,0,0-5.642Zm-4.7,8.463a2.821,2.821,0,0,0-2.648,1.881H11.171a2.821,2.821,0,0,0-2.648-1.881,2.88,2.88,0,0,0-.385.039L5.881,15.2a2.789,2.789,0,0,0,0-3.833L8.138,7.609a2.88,2.88,0,0,0,.385.033,2.821,2.821,0,0,0,2.648-1.881h4.107a2.821,2.821,0,0,0,2.648,1.881,2.88,2.88,0,0,0,.385-.039l2.257,3.761a2.789,2.789,0,0,0,0,3.833l-2.257,3.761A2.88,2.88,0,0,0,17.926,18.926Zm0-15.046a.94.94,0,1,1-.94.94A.94.94,0,0,1,17.926,3.88Zm-9.4,0a.94.94,0,1,1-.94.94A.94.94,0,0,1,8.523,3.88Zm-5.642,9.4a.94.94,0,1,1,.94.94A.94.94,0,0,1,2.881,13.284Zm5.642,9.4a.94.94,0,1,1,.94-.94A.94.94,0,0,1,8.523,22.687Zm9.4,0a.94.94,0,1,1,.94-.94A.94.94,0,0,1,17.926,22.687Zm4.7-8.463a.94.94,0,1,1,.94-.94A.94.94,0,0,1,22.628,14.224Z"
                        transform="translate(0 0)"
                      />
                    </g>
                  </svg>
                }
              >
                {" Extracts "}
                <Link to="/users-extract" />{" "}
              </MenuItem>
            )}
          {adminData?.permissions == "Admin" && (
            <MenuItem
              className={sideBarName === "bioTestes" && "active"}
              onClick={() => handelCurrentLink("bioTestes")}
              icon={<BsCardChecklist />}
            >
              {" Bio-Tests "}
              <Link to="/bio-test-list" />{" "}
            </MenuItem>
          )}
          {console.log("----------------------->", sideBarName)}
          {adminData?.permissions == "Admin" && (
            <MenuItem
              className={sideBarName === "subBioTestes" && "active"}
              onClick={() => handelCurrentLink("subBioTestes")}
              icon={<BsCardChecklist />}
            >
              {" Sub Bio-Tests "}
              <Link to="/sub-bio-test-list" />{" "}
            </MenuItem>
          )}
          {(adminData?.permissions == "Editor" ||
            adminData?.permissions == "Admin") && (
              <MenuItem
                className={sideBarName == "experiment" && "active"}
                onClick={() => handelCurrentLink("experiment")}
                icon={<AiOutlineExperiment />}
              >
                {" Experiments "}
                <Link to="/experiment-list" />
              </MenuItem>
            )}
          <MenuItem
            className={sideBarName == "MedicalIndication" && "active"}
            onClick={() => handelCurrentLink("experiment")}
            icon={<GrCalculator />}
          >
            {" Medical Indications "}
            <Link to="/medical-indication-details" />
          </MenuItem>
          <MenuItem
            className={sideBarName == "Suppliers" && "active"}
            onClick={() => handelCurrentLink("experiment")}
            icon={<FiBox />}
          >
            {" "}
            {intl.formatMessage({ id: "Suppliers" })}{" "}
            <Link to="/suppliers-data" />
          </MenuItem>
        </Menu>
      </SidebarContent>

      <SidebarFooter>
        <div className="sidebar-footer-profile">
          <div className="footer-profile">
            <div className="profile-img">
              {adminData?.user_photo ? (
                <img src={API_URL + adminData?.user_photo} alt="" />
              ) : (
                <img src={ProfileIcon} alt="" />
              )}
            </div>
            <div className="profile-name">
              <strong>{adminData?.username}</strong>
              <span>{adminData?.permissions}</span>
            </div>
          </div>
          <div className="footer-logout-btn">
            <a onClick={() => handleLogOutClicked()}>
              <FiLogOut />
              <p>Logout</p>
            </a>
          </div>
        </div>
      </SidebarFooter>
    </ProSidebar>
  );
};

/**
 * This function manage saveSidebarName in redux
 */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ saveSidebarName, changeFilterStatus }, dispatch),
  };
}

const mapStateToProps = (state) => ({
  sideBarName: state.homeReducer.sideBarName,
  adminData: state.homeReducer.adminData,
});

export default connect(mapStateToProps, mapDispatchToProps)(Aside);
