import axios from "axios";
import { API_URL } from "../config";
import { API_URL2 } from "../config";
export const API_URL_BASE = API_URL;
export const baseUrl = `${API_URL_BASE}`;
export const API_URL_BASE2 = API_URL2;
export const baseUrl2 = `${API_URL_BASE2}`

export const fetchDataFromServer = (
  url,
  type,
  data = null,
  authorization = null,
  Content_Type = false,
) => {
  let header_data = {
    method: type,
    headers: {
      Accept: "application/json",
    },
  };

  if (Content_Type) {
    header_data.headers["Content-Type"] = "application/json";
  }

  if (data) {
    header_data.body = JSON.stringify(data);
  }

  if (authorization) {
    header_data.headers.authorization = "Token " + authorization;
  }
  return fetch(baseUrl + url, header_data)
    .then((response) => {
      let result = {};
      result.header = response;

      if (result.header.status == 401) {
        result.status = 401;
        result.error = "Oops! Session Expired";
        return Promise.reject(result);
      } else {
        return response
          .json()
          .then((json) => {
            result.data = json;
            return result;
          })
          .catch((error) => {
            return result;
          });
      }
    })
    .catch((error) => {
      if (error.status == 401) {
        return Promise.reject(error.error);
      }
    });
};

export const multipartApi = ( 
  url, 
  type,
  formData, 
  authorization = null 
) => {
  return axios({
    method: type,
    url: baseUrl + url,
    data: formData,
    headers: {
      "Authorization": "Token " + authorization,
      'Content-Type': 'multipart/form-data' ,
    }
    })
    .then(function (response) {
        return response.data;
    })
    .catch(function (response) {
        return Promise.reject(response);
    });
}

export const fetchDataFromServer2 =(
  url,
  type,
  data = null,
  authorization = null,
  Content_Type = false,
) => {
  let header_data = {
    method: type,
    headers: {
      Accept: "application/json",
    },
  };

  if (Content_Type) {
    header_data.headers["Content-Type"] = "application/json";
  }

  if (data) {
    header_data.body = JSON.stringify(data);
  }

  if (authorization) {
    header_data.headers.authorization = "Token " + authorization;
  }
  return fetch(baseUrl2  + url, header_data)
    .then((response) => {
      let result = {};
      result.header = response;

      if (result.header.status == 401) {
        result.status = 401;
        result.error = "Oops! Session Expired";
        return Promise.reject(result);
      } else {
        return response
          .json()
          .then((json) => {
            result.data = json;
            return result;
          })
          .catch((error) => {
            return result;
          });
      }
    })
    .catch((error) => {
      if (error.status == 401) {
        return Promise.reject(error.error);
      }
    });
};