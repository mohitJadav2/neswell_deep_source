import { fetchDataFromServer } from "./url";

class Login {
    /**
     * This API manage Login form data
     */
    static LoginForm(formData) {
        return fetchDataFromServer(`/user/login/`, "POST", formData, null, true);
    }
    /**
     *This API manage forgot password data 
    */
    static ForgotPassword(formData) {
        return fetchDataFromServer(`/user/otp-send/`, "POST", formData, null, true);
    }
    /**
     *This API manage OTP verification when user change the passwornd or forgot password 
    */
    static OtpVarification(formData, email) {
        return fetchDataFromServer(`/user/verify-otp/${email}/`, "POST", formData, null, true);
    }
    /**
     * This API manage when user clicked resend button and get new OTP in email address
     */
    static ResetOtp(formData) {
        return fetchDataFromServer(`/user/resend-otp/`, "POST", formData, null, true);
    }
    /**
     * This API manage reset password link to user's email address
     */
    static ResetPassword(formData) {
        return fetchDataFromServer(`/user/forget-password/`, "POST", formData, null, true);
    }
    /**
     * This API manage when user login, That time check the token for varification
     */
    static varifyToken(token) {
        return fetchDataFromServer(`/user/verify_token/${token}/`, "GET", null, null, true);
    }
    /**
     * This API manage reset possword form data
     */
    static ResetPasswordFromLink(uuid, token, formData) {
        return fetchDataFromServer(`users/resetpassword/${uuid}/${token}`, "POST", formData, null, true);
    }
}

export default Login;
