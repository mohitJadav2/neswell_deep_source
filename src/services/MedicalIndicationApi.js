import {
    fetchDataFromServer
} from './url';

class MedicalIndicationApi{
    static MedicalIndicationData(token, page, perPage){
        return fetchDataFromServer(`/medical_indications/?page=${page}&page_size=${perPage}`, "GET", null, token, true);
    }
    static AddMedicalIndication(token,formData) {
        return fetchDataFromServer(`/medical_indication/create/`, "POST", formData, token, true);
    }
    static MedicalIndicationDelete(token, id) {
        return fetchDataFromServer(`/medical_indication/${id}/`, "DELETE", null, token, true);
    }
    static MedicalIndicationTableData(token, id){
        return fetchDataFromServer(`/medical_indication/${id}/`, "GET", null, token, true);
    }
    static AddMedicalIndicationTableData(token,id, formData){
        return fetchDataFromServer(`/medical_indication/${id}/`, "POST",formData, token, true);
    }
    static SelectSubBioTest(token){
        return fetchDataFromServer(`/sub_bio_test_list/`, "GET", null, token, true);
    }
    static SearchMedicalIndicationData(orderType, orderField, token, string, page, perPage){
        return fetchDataFromServer(`/medical_indications/?pera=${orderType}&field=${orderField}&search=${string}&page=${page}&page_size=${perPage} `, "GET", null, token, true);
    }
}
export default MedicalIndicationApi