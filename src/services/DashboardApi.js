import {
    fetchDataFromServer,
    multipartApi,
} from './url';

class DashboardApi {
    /**
     * This API manage when user logout from application 
    */
    static logOutApi(token) {
        return fetchDataFromServer(`/user/logout/`, "GET", null, token, true);
    }

    /**
     * This API manage particular user details for user profile page
     */
    static userProfile(token) {
        return fetchDataFromServer(`/user/manage-profile/`, "GET", null, token, true);
    }

    /**
     * This API manage all user details
     */
    static UsersDetails(formData, token, pageNumber, totalDataToShow, orderType, orderField) {
        return fetchDataFromServer(`/users/?search=${formData}&pera=${orderType}&field=${orderField}&page=${pageNumber}&page_size=${totalDataToShow}`, "GET", null, token, true);
    }

    /**
     * This API manage record display drop down 
     */
    static UsersPerpageChange(fieldType, fieldName, formData, token, totaldataToShow, pageNumber) {
        return fetchDataFromServer(`/users?pera=${fieldType}&field=${fieldName}&search=${formData}&page_size=${totaldataToShow}&page=${pageNumber}`, "GET", null, token, true);
    }
    
    /**
     * This API manage when user create new user 
     */
    static AddUser(formData, token) {
        return fetchDataFromServer(`/users/`, "POST", formData, token, true);
    }

    /**
     * This API manage specific user records
     */
    static UserDetails = async (token, id) => {
        return await fetchDataFromServer(`/users/${id}`, "GET", null, token, true);
    }

    /**
     * This API manage deleted user record
     */
    static UserDelete(token, id) {
        return fetchDataFromServer(`/users/${id}`, "DELETE", null, token, true);
    }

    /**
     * This API manage When user edit the user's record details
     */
    static UserEdit(token, id, formData) {
        return fetchDataFromServer(`/users/${id}/`, "PUT", formData, token, true);
    }
    
    /**
     * This API manage search record in user details page
     */
    static userSearch(fieldType, fieldName, formData, token, totaldataToShow, pageNumber){
        return fetchDataFromServer(`/user/search/?pera=${fieldType}&field=${fieldName}&search=${formData}&page_size=${totaldataToShow}&page=${pageNumber}`, "GET", null, token, true);
    }

    /**
     * This API manager records according to ascending order
     */
    static userAscendingOrder(fieldType, fieldName,totaldataToShow, token){
        return fetchDataFromServer(`/users/?pera=${fieldType}&field=${fieldName}&page_size=${totaldataToShow}`, "GET", null, token, true);
    }

    /** 
     * This API manage perticualr user reset passworn in userprofile page
    */
    static userResetPassword(id, token){
        return fetchDataFromServer(`/users/${id}/reset-password/`, "POST", null, token, true);
    }
    /**
     * This API manage exract data list
     */
    static extractProfile(isFilter, token, pageNumber, totaldataToShow, orderType, orderField, filterData) {
        return fetchDataFromServer(`/extract/records/?filters=${isFilter}&pera=${orderType}&field=${orderField}&page=${pageNumber}&page_size=${totaldataToShow}${filterData ? filterData : ""}`, "GET", null, token, true);
    }
    static extractProfilePagination(isFilter, token, pageNumber, totaldataToShow, orderType, orderField, filterData) {
        return fetchDataFromServer(`/extract/records/?filters=${isFilter}&pera=${orderType}&field=${orderField}&page=${pageNumber}&page_size=${totaldataToShow}${filterData ? filterData : ""}`, "GET", null, token, true);
    }
    static CreateExtractRecord(formData,token) {
        return multipartApi(`/extract/records/`, "post", formData, token);
    }
    static extractHistory(id, token){
        return fetchDataFromServer(`/extract/history/${id}`, "GET", null, token, true);
    }
    static extractSearch(isFilter, formData, token, totaldataToShow, orderType, orderField, pageNumber, filterData){
        return fetchDataFromServer(`/extract/search/?filters=${isFilter}&pera=${orderType}&field=${orderField}&page=${pageNumber}&search=${formData}&page_size=${totaldataToShow}${filterData ? filterData : ""}`, "GET", null, token, true);
    }
    static extractDetail(id, token){
        return fetchDataFromServer(`/extracts/${id}/`, "GET", null, token, true);
    }
    static extractDelete(id, token){
        return fetchDataFromServer(`/extracts/${id}`, "DELETE", null, token, true);
    }
    static extractEditRecord(formData, id, token){
        return multipartApi(`/extracts/${id}/`, "put", formData, token);
    }
    static extractFilter(url, token){
        return fetchDataFromServer(`/extract/records/?filters=True${url}`, "GET", null, token, true);
    }
    static fetchLocationAddress(lat, lng, token){
        return fetchDataFromServer(`/get_location/${lat}/${lng}/`, "GET", null, token, true);
    }
    static fetchSupplierData(token){
        return fetchDataFromServer(`/supplier/extract/data/`, "GET", null, token, true);
    }  
}

export default DashboardApi;
