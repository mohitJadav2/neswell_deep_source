import { combineReducers } from "redux";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import HomeReducer from "./HomeReducer";

const persistConfig = {
    key: "root",
    storage,
    whitelist: ['homeReducer']
}

const rootReducer = combineReducers({
    homeReducer: HomeReducer
})

export default persistReducer(persistConfig, rootReducer );