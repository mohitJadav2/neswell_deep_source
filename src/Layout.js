import React, { useState, useEffect } from 'react';
import Aside from './Aside';
import Main from './Main';
import { IntlProvider } from 'react-intl';
import messages from './messages';
import DashboardApi from './services/DashboardApi';
import { SaveAdminData } from "./Actions/HomeAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function Layout(props) {
  const [rtl, setRtl] = useState(false);
  const [collapsed, setCollapsed] = useState(false);
  const [image, setImage] = useState(true);
  const [toggled, setToggled] = useState(false);
  const [locale, setLocale] = useState('en');
  const [userDetails, setUserDetails] = useState(undefined);

  const handleToggleSidebar = (value) => {
    setToggled(value);
  };

  /**
   * This function manage user profile details from API 
   */
  useEffect(() => {
    DashboardApi.userProfile(localStorage.getItem("neswell_access_token")).then(
      (json) => {
        
            if(!json?.data?.error){
               if(!props?.adminData){
                setUserDetails(json?.data)
              }
            }
        }
    )
  },[])
  useEffect(() => {
    (async () => {
      if(!localStorage.getItem("neswell_access_token")){
        await props?.SaveAdminData(userDetails)
      }
    })();
}, [userDetails]);

  return (
    <div className={`app ${rtl ? 'rtl' : ''} ${toggled ? 'toggled' : ''}`}>
      <IntlProvider locale={locale} messages={messages[locale]}>
        <Aside
          image={image}
          collapsed={collapsed}
          rtl={rtl}
          toggled={toggled}
          handleToggleSidebar={handleToggleSidebar}
          userDetails={userDetails}
        />
        <Main
        >
          {props?.children}
        </Main>
      </IntlProvider>
    </div>
  );
}

/**
 * This function manage SaveAdminData in redux
 */
function mapDispatchToProps(dispatch) {
  return {
      ...bindActionCreators({ SaveAdminData }, dispatch)
  }
}

const mapStateToProps = (state) => ({
  data: state.homeReducer.data,
  adminData: state.homeReducer.adminData,
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout)


