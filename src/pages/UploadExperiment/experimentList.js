import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './experiment.style.scss'
import { FiSearch, } from "react-icons/fi";
import { AiFillCaretDown, AiFillCaretUp, AiOutlineEye } from "react-icons/ai";
import { BsPlusCircle, BsChevronDown, BsChevronUp } from "react-icons/bs";
import { RiDeleteBin6Line } from "react-icons/ri";
import Layout from '../../Layout';
import Pagination from 'react-responsive-pagination';
import UploadExperimentApi from '../../services/UploadExperimentApi';
import * as moment from "moment";
import { useHistory } from "react-router-dom";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import { clearExperimentData } from "../../Actions/HomeAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function ExperimentList(props) {
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(undefined);
    const [totalCount, setTotalCount] = useState(undefined);
    const [page, setpage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [uploadExperimentData, setUploadExperimentData] = useState(undefined);
    const [toggleDropDown, setToggleDropDown] = useState(false);
    const [searchText, setSearchText] = useState(undefined);
    const [modalOpen, setModalOpen] = useState(false);
    const [deleteRecordId, setDeleteRecoredId] = useState(undefined);
    const [loader, setLoader] = useState(false);
    const [orderType, setOrderType] = useState("desc");
    const [orderField, setOrderField] = useState("id");
    const [searchError, setSearchError] = useState(undefined);
    let history = useHistory();

    const perPageValue = [10, 25, 50, 100]

   /**
    * This function manages pagination with passing page number in API
   */
    function handlePageChange(page) {
        setLoader(true)
        if (searchText) {
            setCurrentPage(page);
            UploadExperimentApi.searchUploadExperiment(
                localStorage.getItem("neswell_access_token"),
                page,
                perPage,
                searchText
            ).then(
                async (json) => {
                    if (!json?.data?.error) {
                        let totalPages = json?.data?.count / perPage;
                        setTotalPages(Math.ceil(totalPages));
                        setTotalCount(json?.data?.count);
                        setUploadExperimentData(json?.data?.results)
                        setLoader(false)
                    } else {
                        setLoader(false)
                    }
                }
            )
        } else {
            setLoader(true)
            setCurrentPage(page);
            UploadExperimentApi.fetchExperimentData(
                localStorage.getItem("neswell_access_token"), page, perPage
            ).then(
                async (json) => {
                    if (!json?.data?.error) {
                        let totalPages = json?.data?.count / perPage;
                        setTotalPages(Math.ceil(totalPages));
                        setTotalCount(json?.data?.count);
                        setUploadExperimentData(json?.data?.results)
                        setLoader(false)
                    } else {
                        setLoader(false)
                    }
                }
            )
        }
    }

    /**
     * When user clicked delete button ,this function manage user delete feature 
     * The function called delete Api with user's id 
   */
    const handleDeleteClicked = () => {
        setModalOpen(false);
        setLoader(true);
        if (searchText) {
            UploadExperimentApi.deleteExperiment(
                localStorage.getItem("neswell_access_token"),
                deleteRecordId
            ).then((json) => {
                if (!json.data.error) {
                    UploadExperimentApi.searchUploadExperiment(
                        localStorage.getItem("neswell_access_token"),
                        currentPage,
                        perPage,
                        searchText
                    ).then((json) => {
                        if (!json.data.error) {
                            let totalPages = json?.data?.count / perPage;
                            setTotalPages(Math.ceil(totalPages));
                            setUploadExperimentData(json?.data?.results);
                            setTotalCount(json?.data?.count);
                            setModalOpen(false)
                            setLoader(false);
                        } else {
                            setLoader(false);
                        }
                    });
                } else {
                    setLoader(false);
                }
            });
        } else {
            UploadExperimentApi.deleteExperiment(
                localStorage.getItem("neswell_access_token"),
                deleteRecordId
            ).then((json) => {
                if (!json?.data?.error) {
                    UploadExperimentApi.fetchExperimentData(
                        localStorage.getItem("neswell_access_token"),
                        currentPage, perPage,
                    ).then((res) => {
                        setUploadExperimentData(res?.data?.results);
                        let totalPages = res?.data?.count / perPage;
                        setTotalPages(Math.ceil(totalPages));
                        setTotalCount(res?.data?.count);
                        setLoader(false);
                    });
                } else {
                    setLoader(false);
                }
            });

        }
    };

    /**
     * This function fetch experiment Data from API
    */
    useEffect(() => {
        setLoader(true)
        UploadExperimentApi.fetchExperimentData(localStorage.getItem("neswell_access_token"), page, perPage).then(
            async (json) => {
                if (!json?.data?.error) {
                    let totalPages = json?.data?.count / perPage;
                    setTotalPages(Math.ceil(totalPages));
                    setTotalCount(json?.data);
                    setUploadExperimentData(json?.data?.results)
                    setLoader(false)
                } else {
                    setLoader(false)
                }
            }
        )
    }, [])

    
    /**
     * This function manage search feature 
     * This function passes user's enter value to API and get positive and error response
    */
    const handleSearch = (value) => {
        setSearchText(value)
        UploadExperimentApi.searchUploadExperiment(
            localStorage.getItem("neswell_access_token"),
            1,
            perPage,
            value
        ).then(
            async (json) => {
                if (!json?.data?.error) {
                    let totalPages = json?.data?.count / perPage;
                    setTotalPages(Math.ceil(totalPages));
                    setTotalCount(json?.data?.count);
                    setUploadExperimentData(json?.data?.results)
                    if (json?.data?.results?.length == 0) {
                        setSearchError("No data available");
                    } else {
                        setSearchError(undefined)
                    }
                } else {
                }
            }
        )
    }

    /**
     * This function manages record display drop down 
     * When user change value of drop down at that time  this function call API with change value
    */
    const handlePerPageChanges = async (perPageNuumber) => {
        setLoader(true)
        setToggleDropDown(false)
        setPerPage(perPageNuumber)
        if (searchText) {
            await UploadExperimentApi.searchUploadExperiment(
                localStorage.getItem("neswell_access_token"),
                currentPage,
                perPageNuumber,
                searchText
            ).then(
                async (json) => {
                    if (!json?.data?.error) {
                        let totalPages = json?.data?.count / perPageNuumber;
                        setTotalPages(Math.ceil(totalPages));
                        setTotalCount(json?.data?.count);
                        setUploadExperimentData(json?.data?.results)
                        setLoader(false)
                    } 
                }
            )
        } else {
            setLoader(true)
            await UploadExperimentApi.fetchExperimentData(
                localStorage.getItem("neswell_access_token"), page, perPageNuumber
            ).then(
                async (json) => {
                    if (!json?.data?.error) {
                        let totalPages = json?.data?.count / perPageNuumber;
                        setTotalPages(Math.ceil(totalPages));
                        setTotalCount(json?.data?.count);
                        setUploadExperimentData(json?.data?.results)
                        setLoader(false)
                    } 
                }
            )
        }
    }
    
    /**
     * This function manage user record column's ascending and descending order
    */
    const handleAscending = (fieldName) => {
        setOrderType("asc");
        setOrderField(fieldName);
        setLoader(true);
        if (searchText) {
            UploadExperimentApi.searchUploadExperiment(
                "asc",
                orderField,
                localStorage.getItem("neswell_access_token"),
                page,
                perPage
            ).then((json) => {
                if (!json.data.error) {
                    let totalPages = json?.data?.count / perPage;
                    setTotalPages(Math.ceil(totalPages));
                    setUploadExperimentData(json?.data?.results);
                    setTotalCount(json?.data?.count);
                } else {
                    setLoader(false);
                }
            });
        } else {
            UploadExperimentApi.AssendingfetchExperimentData(
                "asc",
                orderField,
                localStorage.getItem("neswell_access_token"),
                page,
                perPage
            ).then((json) => {
                if (!json?.data?.error) {
                    let totalPages = json?.data?.count / perPage;
                    setTotalPages(Math.ceil(totalPages));
                    setTotalCount(json?.data?.count);
                    setUploadExperimentData(json?.data?.results);
                    setLoader(false);
                } else {
                    setLoader(false);
                }
            });
        }
    };
    
    const handledescending = (fieldName) => {
        setOrderType("desc");
        setOrderField(fieldName);
        setLoader(true);
        if (searchText) {
            UploadExperimentApi.searchUploadExperiment(
                "desc",
                orderField,
                localStorage.getItem("neswell_access_token"),
                page,
                perPage
            ).then((json) => {
                if (!json.data.error) {
                    let totalPages = json?.data?.count / perPage;
                    setTotalPages(Math.ceil(totalPages));
                    setUploadExperimentData(json?.data?.results);
                    setTotalCount(json?.data?.count);
                } else {
                    setLoader(false);
                }
            });
        } else {
            UploadExperimentApi.AssendingfetchExperimentData(
                "desc",
                orderField,
                localStorage.getItem("neswell_access_token"),
                page,
                perPage
            ).then((json) => {
                if (!json?.data?.error) {
                    let totalPages = json?.data?.count / perPage;
                    setTotalPages(Math.ceil(totalPages));
                    setTotalCount(json?.data?.count);
                    setUploadExperimentData(json?.data?.results);
                    setLoader(false);
                } else {
                    setLoader(false);
                }
            });
        }
    };


    return (
        <Layout>
            {loader && <Loader />}
            <Modal
                open={modalOpen}
                closeModal={() => setModalOpen(false)}
                DeleteUsersRecord={() => handleDeleteClicked()}
                title={"Are you sure?"}
                subTitle={"Do you want to delete this record?"}
                delete={true}
            />
            <div className="users-details-wrapper">
                <div className="users-details-inner-wrapper">
                    <div className="user-search">
                        <div className="search">
                            <button className="search-icon"> <FiSearch /> </button>
                            <input
                                onChange={(e) => handleSearch(e.target.value)}
                                type="search"
                                placeholder="Search"
                                className="search-input"
                                value={searchText}
                            />
                        </div>
                        <div className="add-user-btn" onClick={() => props?.clearExperimentData()}>
                            <Link to="/experiment-data" className="primary-btn"> <BsPlusCircle /> Create Experiment</Link>
                        </div>
                    </div>
                    <div className="user-details-table details-table">
                        <table>
                            <thead>
                                <tr>
                                    <th>
                                        <div onClick={() => { orderType == "asc" ? handledescending("id") : handleAscending("id") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                                            Upload ID
                                            <div style={{ marginLeft: 10, height: 22, }}>
                                                <div style={{ cursor: "pointer", marginBottom: -8, }}><AiFillCaretUp style={{ opacity: orderField == "id" && orderType == "asc" ? 0.5 : 1 }} /></div>
                                                <div style={{ cursor: "pointer", marginTop: -8, }}><AiFillCaretDown style={{ opacity: orderField == "id" && orderType == "desc" ? 0.5 : 1 }} /></div>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div onClick={() => { orderType == "asc" ? handledescending("id") : handleAscending("id") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                                            Name
                                            <div style={{ marginLeft: 10, height: 22, }}>
                                                <div style={{ cursor: "pointer", marginBottom: -8, }}><AiFillCaretUp style={{ opacity: orderField == "id" && orderType == "asc" ? 0.5 : 1 }} /></div>
                                                <div style={{ cursor: "pointer", marginTop: -8, }}><AiFillCaretDown style={{ opacity: orderField == "id" && orderType == "desc" ? 0.5 : 1 }} /></div>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div onClick={() => { orderType == "asc" ? handledescending("id") : handleAscending("id") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                                            Uploaded by
                                            <div style={{ marginLeft: 10, height: 22, }}>
                                                <div style={{ cursor: "pointer", marginBottom: -8, }}><AiFillCaretUp style={{ opacity: orderField == "id" && orderType == "asc" ? 0.5 : 1 }} /></div>
                                                <div style={{ cursor: "pointer", marginTop: -8, }}><AiFillCaretDown style={{ opacity: orderField == "id" && orderType == "desc" ? 0.5 : 1 }} /></div>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div onClick={() => { orderType == "asc" ? handledescending("id") : handleAscending("id") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                                            Date & Time
                                            <div style={{ marginLeft: 10, height: 22, }}>
                                                <div style={{ cursor: "pointer", marginBottom: -8, }}><AiFillCaretUp style={{ opacity: orderField == "id" && orderType == "asc" ? 0.5 : 1 }} /></div>
                                                <div style={{ cursor: "pointer", marginTop: -8, }}><AiFillCaretDown style={{ opacity: orderField == "id" && orderType == "desc" ? 0.5 : 1 }} /></div>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div onClick={() => { orderType == "asc" ? handledescending("id") : handleAscending("id") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                                            Action
                                            <div style={{ marginLeft: 10, height: 22, }}>
                                                <div style={{ cursor: "pointer", marginBottom: -8, }}><AiFillCaretUp style={{ opacity: orderField == "id" && orderType == "asc" ? 0.5 : 1 }} /></div>
                                                <div style={{ cursor: "pointer", marginTop: -8, }}><AiFillCaretDown style={{ opacity: orderField == "id" && orderType == "desc" ? 0.5 : 1 }} /></div>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody style={{ cursor: "pointer" }}>
                                {uploadExperimentData?.map((item) => (
                                    <tr>
                                        <td onClick={() => history.push(`/view-experiment/${item?.id}`)}>{item?.id}</td>
                                        <td onClick={() => history.push(`/view-experiment/${item?.id}`)}>{item?.name ? item?.name : "-"}</td>
                                        <td onClick={() => history.push(`/view-experiment/${item?.id}`)}>{item?.user_id}</td>
                                        <td onClick={() => history.push(`/view-experiment/${item?.id}`)}>{moment(item?.modified).format('ddd, DD MMM YYYY HH:mm:ss')}</td>
                                        <td>
                                            <span onClick={() => history.push(`/view-experiment/${item?.id}`)} className='gray-color-eyes' style={{ cursor: 'pointer' }}>
                                                <AiOutlineEye />
                                            </span>
                                            <span onClick={() => {
                                                setModalOpen(true)
                                                setDeleteRecoredId(item?.id)
                                            }} className="blue-color-eyes">
                                                < RiDeleteBin6Line />
                                            </span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <p style={{ textAlign: "center" }}>{searchError}</p>
                    <div className="pagination-wrapper">
                        <div className="entries-content">
                            <span>{`Showing 1 to ${uploadExperimentData?.length} of ${totalCount?.count == undefined ? totalCount : totalCount?.count} entries`}</span>
                        </div>
                        <div className="pagination">
                            <Pagination
                                total={totalPages}
                                current={currentPage}
                                onPageChange={page => handlePageChange(page)}
                            />
                        </div>
                        <div className='per-pagination-wrapper'>
                            <div onClick={() => setToggleDropDown(!toggleDropDown)} className={toggleDropDown ? 'selected-page open' : 'selected-page '}>
                                <p>{perPage}</p>
                                <div className='up-down-arrow'>
                                    <span className='down-arrow'><BsChevronDown /></span>
                                    <span className='up-arrow'><BsChevronUp /></span>
                                </div>
                            </div>
                            {toggleDropDown && <ul className='per-page-dropdown'>
                                {perPageValue?.map((item) => (
                                    <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                                ))}
                            </ul>}
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ clearExperimentData }, dispatch)
    }
}

export default connect(null, mapDispatchToProps)(ExperimentList)

