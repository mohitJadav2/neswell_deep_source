import React from "react";
import Logo from '../../assets/images/Login/login-logo.svg';
import PageNotFoundZip from '../../assets/images/page-not-found.gif';
import { Link } from "react-router-dom";
import './404.style.scss';
import Layout from '../../Layout'

function pageNotFound() {
    return (
        <Layout>
            <section className="page-not-found-bg">
                <div className="page-not-found-wrapper">
                    <div className="page-not-found-logo">
                        <img src={Logo} alt="" />
                    </div>
                    <div className="page-not-found-zip">
                        <img src={PageNotFoundZip} alt="" />
                    </div>
                    <div className="page-not-found-bottom-text">
                        <h2>Oops</h2>
                        <p>We couldn't find this page</p>
                        <div className="back-home-btn">
                            <Link to="/">Back to Home</Link>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    )
}
export default pageNotFound;