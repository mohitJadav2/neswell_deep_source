import React, { useState, useEffect } from "react";
import Layout from "../../Layout";
import "./bioTest.style.scss";
import { FiSearch } from "react-icons/fi";
import { AiOutlinePlus, AiOutlineEye } from "react-icons/ai";
import { BsGrid } from "react-icons/bs";
import { BsListUl } from "react-icons/bs";
import ArrowRight from "../../assets/icons/arrow-blue.svg";
import Pagination from "react-responsive-pagination";
import BioTestApi from "../../services/BioTestApi";
import { saveFatherBioTests } from "../../Actions/HomeAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import Loader from "../../components/loader";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";

function BioTestList(props) {
  const [view, setView] = useState("Grid");
  const [totalPages, setTotalPages] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [parentBioTestes, setParentBioTestes] = useState(undefined);
  const [totalCount, setTotalCount] = useState(undefined);
  const [mouseHoverEvent, setMouseHoverEvent] = useState(undefined);
  const [loader, setLoader] = useState(false);
  let history = useHistory();
  const [totaldataToShow, setTotaldataToShow] = useState(10);
  const [perPage, setPerPage] = useState(10);
  const [toggleDropDown, setToggleDropDown] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [error, setError] = useState(undefined)
  const perPageValue = [10, 25, 50, 100];


  /** 
   * this style is use for the selection fild
  */
  const customSelectStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? "#fff" : "#404040",
      padding: 10,
      borderRadius: 10,
      width: "98%",
      margin: "10px auto",
    }),
    control: (styles) => ({ ...styles, border: 'solid 1px #E5E5E5', backgroundColor: 'white', maxHeight: 46, minHeight: 46, fontSize: 14, borderRadius: 10, color: '#929292', fontWeight: '600' }),
  };

  

  /** 
   * This useEffect() manage bioTest records
   */
  useEffect(() => {
    setLoader(true);
    BioTestApi.fatherParentBioTest(
      localStorage.getItem("neswell_access_token"),
      currentPage,
      perPage
    ).then((json) => {
      if (!json?.data?.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setTotalCount(json?.data?.count);
        setParentBioTestes(json?.data?.results);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }, []);

  /**
    * This function manages pagination with passing page number in API
   */
  function handlePageChange(page) {
    setLoader(true);
    setCurrentPage(page);
    BioTestApi.fatherParentBioTest(
      localStorage.getItem("neswell_access_token"),
      page,
      perPage
    ).then((json) => {
      if (!json.data.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setParentBioTestes(json?.data?.results);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }

  /**
     * This function manage search feature 
     * This function passes user's enter value to API and get positive and error response
  */
  const handleSearch = (value) => {
    BioTestApi.searchFatherBioTest(
      localStorage.getItem("neswell_access_token"),
      value,
      perPage
    ).then((json) => {
      if (!json.data.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setParentBioTestes(json?.data?.results);
      } else {
        setError(json?.data?.error)
      }
    });
  };

  /*
    *This function redirect to sub bio test details page
  */
  const handleSaveData = (item) => {
    history.push(`/bio-test-view/${item?.id}`);
  };
   /*
    *This function redirect to create sub bio test page
  */
  const handleCreatClick = () => {
    history.push(`/create-bio`)
  }
   /**
     * This function manages record display drop down 
     * When user change value of drop down at that time  this function call API with change value
    */
  const handlePerPageChanges = async (perPageNuumber) => {
    setToggleDropDown(false)
    setPerPage(perPageNuumber)
    if (searchText) {
      await BioTestApi.fatherParentBioTest(
        localStorage.getItem("neswell_access_token"),
        currentPage,

      ).then(
        async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setParentBioTestes(json?.data?.results)
          }
        }
      )
    } else {
      await BioTestApi.fatherParentBioTest(
        localStorage.getItem("neswell_access_token"),
        currentPage,
        perPage,
        perPageNuumber
      ).then(
        async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setParentBioTestes(json?.data?.results)
          }
        }
      )
    }
  }

  return (
    <Layout>
      {loader && <Loader />}
      <div className="bio-test-list-wrapper">
        <div className="bio-test-title-list-icon">
          <h2>Bio-Tests ({parentBioTestes?.length})</h2>
          <div className="list-grid-icon">
            <button
              onClick={() => setView("Grid")}
              className={
                view == "Grid"
                  ? "grid-icon-btn grid-list-icon active"
                  : "grid-icon-btn grid-list-icon"
              }
            >
              {" "}
              <BsGrid />{" "}
            </button>
            <button
              onClick={() => setView("List")}
              className={
                view == "List"
                  ? "list-icon-btn grid-list-icon active"
                  : "list-icon-btn grid-list-icon"
              }
            >
              {" "}
              <BsListUl />{" "}
            </button>
          </div>
        </div>
        <div className="bio-test-search">
          <div className="search">
            <button className="search-icon">
              {" "}
              <FiSearch />{" "}
            </button>
            <input
              onChange={(e) => handleSearch(e.target.value)}
              type="search"
              placeholder="Search"
              className="search-input"
            />
          </div>
        </div>
        <div className="grid-view-main-wrapper">
          {view == "Grid" && (
            <div className="grid-view-inner-wrapper">
              {parentBioTestes?.length > 0 ? (
                parentBioTestes?.map((item) => (
                  <div
                    onMouseEnter={() => setMouseHoverEvent(item.id)}
                    onMouseLeave={() => setMouseHoverEvent(undefined)}
                    onClick={() => handleSaveData(item)}
                    className="grid-view-column"
                  >
                    <div className="grid-title-right-icon">
                      <div className="title">{item?.name}</div>
                      <div>
                        {" "}
                        <img src={ArrowRight} alt="" />{" "}
                      </div>
                    </div>
                    <div className="grid-title-bottom-icon">
                      <div className="child-title" >
                        <p onClick={() => handleSaveData(item)}>Sub Bio-Tests</p>
                        <span>{item?.child_bio_count}</span>
                      </div>
                      <div className="pluse-link" onClick={() => handleCreatClick()}>
                        <AiOutlinePlus />
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <p style={{ textAlignt: 'center' }}>No records found</p>
              )}
              <div className="pagination-wrapper">
                <div className="entries-content">
                  <span>{`Showing 1 to ${parentBioTestes?.length} of ${totalCount} entries`}</span>
                </div>
                <div className="pagination">
                  <Pagination
                    total={totalPages}
                    current={currentPage}
                    onPageChange={(page) => handlePageChange(page)}
                  />
                </div>
                <div className="per-pagination-wrapper">
                  <div
                    onClick={() => setToggleDropDown(!toggleDropDown)}
                    className={
                      toggleDropDown ? "selected-page open" : "selected-page "
                    }
                  >
                    <p>{perPage}</p>
                    <div className="up-down-arrow">
                      <span className="down-arrow">
                        <BsChevronDown />
                      </span>
                      <span className="up-arrow">
                        <BsChevronUp />
                      </span>
                    </div>
                  </div>
                  {toggleDropDown && (
                    <ul className="per-page-dropdown">
                      {perPageValue?.map((item) => (
                        <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
            </div>
          )}
          {view == "List" && (
            <div className="list-view-inner-wrapper">
              <div className="list-view-table details-table">
                <table>
                  <thead>
                    <tr>
                      <th style={{ width: "10%" }}>Sr.</th>
                      <th style={{ width: "65%" }}>Bio-Tests</th>
                      <th style={{ width: "15%", textAlign: "center" }}>
                        Sub Bio-Tests
                      </th>
                      <th style={{ width: "10%", textAlign: "center" }}>
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      parentBioTestes?.map((item) => (
                        <tr
                          style={{ cursor: "pointer" }}
                          onClick={() => handleSaveData(item)}
                        >
                          <td>{`#${item?.id}`}</td>
                          <td>{item?.name}</td>
                          <td style={{ textAlign: "center" }}>
                            {item?.child_bio_count}
                          </td>
                          <td style={{ textAlign: "center" }} className="text">
                            {" "}
                            <span className="blue-color-eyes">
                              <AiOutlineEye />
                            </span>{" "}
                          </td>
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
                {parentBioTestes.length == 0 && <p style={{ textAlign: 'center' }}>No records found</p>}
              </div>
              <div className="pagination-wrapper">
                <div className="entries-content">
                  <span>{`Showing 1 to ${parentBioTestes?.length} of ${totalCount} entries`}</span>
                </div>
                <div className="pagination">
                  <Pagination
                    total={totalPages}
                    current={currentPage}
                    onPageChange={(page) => handlePageChange(page)}
                  />
                </div>
                <div className="per-pagination-wrapper">
                  <div
                    onClick={() => setToggleDropDown(!toggleDropDown)}
                    className={
                      toggleDropDown ? "selected-page open" : "selected-page "
                    }
                  >
                    <p>{perPage}</p>
                    <div className="up-down-arrow">
                      <span className="down-arrow">
                        <BsChevronDown />
                      </span>
                      <span className="up-arrow">
                        <BsChevronUp />
                      </span>
                    </div>
                  </div>
                  {toggleDropDown && (
                    <ul className="per-page-dropdown">
                      {perPageValue?.map((item) => (
                        <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </Layout>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ saveFatherBioTests }, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(BioTestList);
