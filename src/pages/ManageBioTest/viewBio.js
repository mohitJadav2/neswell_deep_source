import React, { useState, useEffect } from "react";
import "./bioTest.style.scss";
import { MdContentCopy } from "react-icons/md";
import { AiOutlineMinus } from "react-icons/ai";
import { BsPlusCircle } from "react-icons/bs";
import { RiDeleteBin5Fill } from "react-icons/ri";
import Layout from "../../Layout";
import BioTestApi from "../../services/BioTestApi";
import { useHistory, useLocation } from "react-router-dom";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";

function ViewBio(props) {
  const [childBioTestData, setChildBioTestData] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [clusterList, setClusterList] = useState([])
  const [supportedExtract, setSupportedExtract] = useState([])
  const [fatherBioTest, setfatherBioTest] = useState([])
  const [extractError,setExtractError] = useState(false)
  let history = useHistory();
  const location = useLocation();
  console.log("location------->",location?.state)


  /**
   * This function is use for get sub bio test details from api 
  */
  useEffect(() => {
    setLoader(true)
    BioTestApi.DeleteAndDetailChildBioTest(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug,
      "GET"
    ).then((json) => {
      if (!json?.data?.error) {
        console.log("json-------->",json)
        setChildBioTestData(json?.data);
        setClusterList(json?.data?.clusters)
        setfatherBioTest(json?.data?.general_bio_test_id)
        setSupportedExtract(json?.data?.suppoerted_extract)
        if(json?.data?.suppoerted_extract?.length == 0){
          console.log("this is error-------")
          setExtractError(true)
        }else{
          setExtractError(false)
        }
        if(json?.data?.scoring_graph?.length == 0){
          console.log("this is error-------")
          setExtractError(true)
        }else{
          setExtractError(false)
        }
        setLoader(false)
      } else {
        setLoader(false)
      }
    });
  }, []);

  /**This function is use for delete sub bio test record */
  const handleDeleteChild = () => { 
    setLoader(true)
    BioTestApi.DeleteAndDetailChildBioTest(
      localStorage.getItem("neswell_access_token"),
      deleteUserData,
      "DELETE"
    ).then((json) => {
      if (!json?.data?.error) {
        if(location?.state == undefined){
        history.push(`/bio-test-view/${fatherBioTest?.value}`);
      }else{
        history.push(`/sub-bio-test-list`)
      }
        setLoader(false)
      } else {
        setLoader(false)
      }
    });
  };
  /**This function is use for when user clicked on Duplicate sub bio test button and user redirect to create sub bio test page*/
  const handleDuplicateChild = (id) => {
    history.push({
      pathname: `/create-bio/${id}`,
      state: { userDetail: fatherBioTest?.value },
    });
  };

  /**This function is use for when user clicked on create sub bio test button and user redirect to create sub bio test page*/
  const handleCreatChildBioTest = () => {
    history.push({
      pathname: `/create-bio`,
      state: { userDetail: fatherBioTest?.value },
    });
  };

  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => handleDeleteChild()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={deleteModal}
      />
      <div className="add-users-details-wrapper">
        <div className="users-title">
          <h2>Sub Bio Test</h2>
        </div>
        <div className="view-bio-details-wrapper">
          <div className="view-bio-details">
            <div className="view-bio-details-item">
              <span>Sub Bio Test</span>
              <p>{childBioTestData?.name}</p>
            </div>
            <div className="view-bio-details-item">
              <span>Bio-Test</span>
              <p>{fatherBioTest?.label}</p>
            </div>
            <div className="view-bio-details-item">
              <span>Experimental Condition</span>
              <div className="clusters-option">
                <p>
                {clusterList.map((item) => (
                  <span>
                      <AiOutlineMinus />
                      {item?.label}
                    </span>
                  ))}
                </p>
              </div>
            </div>
            <div className="view-bio-details-item">
              <span>Suppoerted Extract</span>
              <div className="clusters-option">
                <p>
                {!extractError&& supportedExtract.map((item) => (
                  <span>
                      <AiOutlineMinus />
                      {item}
                    </span>
                  ))}
                  {extractError && <span>Data is not available.</span>}
                </p>
              </div>
            </div>
            <div className="view-bio-details-item">
              <span>Scoring Graph</span>
              <div className="clusters-option">
                <p>
                {!extractError && clusterList.map((item) => (
                  <span>
                      Data is not available.
                      {/* <AiOutlineMinus />
                      {item?.label} */}
                    </span>
                 ))}
                  {extractError && <span>Data is not available.</span>}
                </p>
              </div>
            </div>
            <div className="view-bio-details-btn">
              <button
                onClick={() => handleCreatChildBioTest(childBioTestData?.id)}
                className="btn create"
              >
                <BsPlusCircle /> Create Sub Bio Test
              </button>
              <button
                onClick={() => handleDuplicateChild(childBioTestData?.id)}
                className="btn duplicate"
              >
                <MdContentCopy /> Duplicate
              </button>
              <button
                onClick={() => {
                  setDeleteModal(true);
                  setModalOpen(true);
                  setDeleteUserData(childBioTestData?.id);
                }}
                className="btn delete"
              >
                <RiDeleteBin5Fill /> Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default ViewBio;
