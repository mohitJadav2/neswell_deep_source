import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import "./users.style.scss";
import { FiSearch } from "react-icons/fi";
import {AiOutlineSetting, AiFillCaretDown, AiFillCaretUp } from "react-icons/ai";
import { BsPlusCircle } from "react-icons/bs";
import Layout from "../../Layout";
import Pagination from "react-responsive-pagination";
import DashboardApi from "../../services/DashboardApi";
import LoginApi from "../../services/LoginApi";
import { useHistory } from "react-router-dom";
import { SaveEditData } from "../../Actions/HomeAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";

/**
 * This function manages user details List
 */
function Users(props) {
  const [currentPage, setCurrentPage] = useState(1);
  const [page, setPage] = useState(1);
  const [userData, setUserData] = useState(undefined);
  const [totalPages, setTotalPages] = useState(undefined);
  const [totalCount, setTotalCount] = useState(undefined);
  const [openDropDown, setOpenDropDown] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [perPage, setPerPage] = useState(10);
  const [orderType, setOrderType] = useState("desc");
  const [orderField, setOrderField] = useState("id");
  const [searchText, setSearchText] = useState("");
  const [toggleDropDown, setToggleDropDown] = useState(false);
  const [searchError, setSearchError] = useState(undefined)
  let history = useHistory();
  const perPageValue = [10, 25, 50, 100]


  /**
   * When user clicked outside of the alert popup box this function executed 
   * The function manages mouse event
  */
  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenDropDown(undefined);
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  /** 
   * The uesEffect function use to collect all user's data from API and display to UI
  */
  useEffect(() => {
    setLoader(true)
    LoginApi.varifyToken(localStorage.getItem("neswell_access_token")).then(
      (json) => {
        if (json?.data?.status) {
          DashboardApi.UsersDetails(
            searchText,
            localStorage.getItem("neswell_access_token"),
            page,
            perPage,
            orderType,
            orderField
          ).then((json) => {
            if (!json?.data?.error) {
              let totalPages = json?.data?.count / perPage;
              setTotalPages(Math.ceil(totalPages));
              setTotalCount(json?.data?.count);
              setUserData(json?.data?.results);
              setLoader(false);

            } else {
              setLoader(false);
            }
          });
        } else {
          localStorage.setItem("neswell_access_token", "null");
          history.push(`/`);
        }
      }
    );
  }, []);

  /** 
   * The function manages action popUp box for each user records 
  */
  const handleSettingClick = async (item) => {
    openDropDown == item?.id
      ? setOpenDropDown(undefined)
      : setOpenDropDown(item?.id);
  };

  /**
   * This function is used for native gate to the user detail page
   */
  const handleUserClicked = async (item) => {
    await props?.SaveEditData(item);
    history.push(`/users-profile/${item?.id}`);
  };

  /**
   * This function manage user edit feature 
   * To collect specific user record the function passes user's id
   */
  const handleEditClicked = async (item) => {
    await props?.SaveEditData(item);
    history.push(`add-users/${item?.id}`);
  };

  /**
   * The function redirect to create new user page with empty string
   */
  const handleAdduser = async () => {
    await props?.SaveEditData(undefined);
    history.push("/add-users");
  };

  /**
   * The function manage reset password feature 
   * The function send re-set password link to user's email address
   */
  const handleResetPassword = async (item) => {
    setLoader(true);
    DashboardApi.userResetPassword(
      item?.id,
      localStorage.getItem("neswell_access_token")
    ).then((json) => {
      if (!json.data.error) {
        setDeleteModal(false);
        setModalOpen(true);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  };

  /**
   * When user clicked delete button ,this function manage user delete feature 
   * The function called delete Api with user's id 
   */
  const handeleDeleteClicked = () => {
    setModalOpen(false);
    setLoader(true);
    if (searchText) {

      DashboardApi.UserDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData?.id
      ).then((json) => {
        if (!json.data.error) {
          DashboardApi.userSearch(
            orderType,
            orderField,
            searchText,
            localStorage.getItem("neswell_access_token"),
            perPage,
            currentPage
          ).then((json) => {
            if (!json.data.error) {
              let totalPages = json?.data?.count / perPage;
              setTotalPages(Math.ceil(totalPages));
              setUserData(json?.data?.results);
              setTotalCount(json?.data?.count);
              setLoader(false);
            } else {
              setLoader(false);
            }
          });
        } else {
          setLoader(false);
        }
      })

    } else {
      DashboardApi.UserDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData?.id
      ).then((json) => {
        if (!json.data.error) {
          DashboardApi.UsersDetails(
            searchText,
            localStorage.getItem("neswell_access_token"),
            currentPage,
            perPage,
            orderType,
            orderField
          ).then((json) => {
            if (!json.data.error) {
              let totalPages = json?.data?.count / perPage;
              setTotalPages(Math.ceil(totalPages));
              setTotalCount(json?.data?.count);
              setUserData(json?.data?.results);
              setLoader(false);
            } else {
              setLoader(false);
            }
          })
        } else {
          setLoader(false);
        }
      })
    }
  };

  /**
   * This function manages pagination with passing page number in API
   */
  function handlePageChange(page) {
    setLoader(true);
    setCurrentPage(page);
    if (searchText) {
      DashboardApi.userSearch(
        orderType,
        orderField,
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPage,
        page
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setTotalCount(json?.data?.count);
          setLoader(false);
        } else {
          setLoader(false);
        }
      })
    } else {
      DashboardApi.UsersDetails(
        searchText,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage,
        orderType,
        orderField
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setTotalCount(json?.data?.count);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  }

  /**
   * This function manage search feature 
   * This function passes user's enter value to API and get positive and error response
  */
  const handelSearch = async (searchText) => {
    setSearchText(searchText)
    setCurrentPage(1);
    DashboardApi.userSearch(
      orderType,
      orderField,
      searchText,
      localStorage.getItem("neswell_access_token"),
      perPage,
      1
    ).then((json) => {
      if (!json.data.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setTotalCount(json?.data?.count);
        setUserData(json?.data?.results);
        setSearchError(json?.data?.message)
      } 
    });
  };

  /**
   * This function manages record display drop down 
   * When user change value of drop down at that time  this function call API with change value
   */
  const handlePerPageChanges = async (perPageNuumber) => {
    setToggleDropDown(false)
    setPerPage(perPageNuumber)
    if (searchText) {
      await DashboardApi.userSearch(
        orderType,
        orderField,
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPageNuumber
      ).then(
        async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setUserData(json?.data?.results)
          } else {
          }
        }
      )
    } else {
      await DashboardApi.UsersPerpageChange(
        orderType,
        orderField,
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPageNuumber,
        page
      ).then(
        async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setUserData(json?.data?.results)
          } else {
          }
        }
      )
    }
  }

  /**
   * This function manage user record column's ascending and descending order
   */
  const handleAscending = (fieldName) => {
    setOrderType("aesc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      DashboardApi.userSearch(
        "aesc",
        fieldName,
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPage,
        1
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setTotalCount(json?.data?.count);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      DashboardApi.userAscendingOrder(
        "aesc",
        fieldName,
        perPage,
        localStorage.getItem("neswell_access_token")
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  }
 
  const handledescending = (fieldName) => {
    setOrderType("desc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      DashboardApi.userSearch(
        "desc",
        fieldName,
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPage,
        1
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setTotalCount(json?.data?.count);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      DashboardApi.userAscendingOrder(
        "desc",
        fieldName,
        perPage,
        localStorage.getItem("neswell_access_token")
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      })
    }
  }

  /**this ref is for the when user clicked outside the fucntion */
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);
  return (
    <Layout>
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => {
          deleteModal ? handeleDeleteClicked() : setModalOpen(false);
        }}
        title={"Are you sure?"}
        subTitle={
          deleteModal
            ? "Do you want to delete this record?"
            : "we have emailed user password reset link!"
        }
        delete={deleteModal}
      />
      {loader && <Loader />}
      <div className="users-details-wrapper">
        <div className="users-details-inner-wrapper">
          <div className="user-search">
            <div className="search">
              <button className="search-icon">
                {" "}
                <FiSearch />{" "}
              </button>
              <input
                onChange={(e) => handelSearch(e.target.value)}
                type="search"
                placeholder="Search"
                className="search-input"
                value={searchText}
              />
            </div>
            <div className="add-user-btn">
              <Link onClick={() => handleAdduser()} className="primary-btn">
                {" "}
                <BsPlusCircle /> Add New User
              </Link>
            </div>
          </div>
          <div className="user-details-table details-table">
            <table>
              <thead>
                <tr>
                  <th>
                    <div onClick={() => { orderType == "aesc" ? handledescending("id") : handleAscending("id") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                      Sr.
                      <div style={{ marginLeft: 10, height: 22, }}>
                        <div style={{ cursor: "pointer", marginBottom: -8, }}><AiFillCaretUp style={{ opacity: orderField == "id" && orderType == "aesc" ? 0.5 : 1 }} /></div>
                        <div style={{ cursor: "pointer", marginTop: -8, }}><AiFillCaretDown style={{ opacity: orderField == "id" && orderType == "desc" ? 0.5 : 1 }} /></div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div onClick={() => { orderType == "aesc" ? handledescending("username") : handleAscending("username") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                      User Name
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}><AiFillCaretUp style={{ opacity: orderField == "username" && orderType == "desc" ? 0.5 : 1 }} /></div>
                        <div style={{ cursor: "pointer", marginBottom: -8, }}><AiFillCaretDown style={{ opacity: orderField == "username" && orderType == "aesc" ? 0.5 : 1 }} /></div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div onClick={() => { orderType == "aesc" ? handledescending("email") : handleAscending("email") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                      Email
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}><AiFillCaretUp style={{ opacity: orderField == "email" && orderType == "desc" ? 0.5 : 1 }} /></div>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}><AiFillCaretDown style={{ opacity: orderField == "email" && orderType == "aesc" ? 0.5 : 1 }} /></div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div onClick={() => { orderType == "aesc" ? handledescending("permissions") : handleAscending("permissions") }} style={{ display: 'flex', alignItems: 'center', cursor: "pointer" }}>
                      User Type
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}><AiFillCaretUp style={{ opacity: orderField == "permissions" && orderType == "desc" ? 0.5 : 1 }} /></div>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}><AiFillCaretDown style={{ opacity: orderField == "permissions" && orderType == "aesc" ? 0.5 : 1 }} /></div>
                      </div>
                    </div>
                  </th>
                  <th style={{ cursor: "context-menu" }}>Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {userData?.map((item, idx) => {
                  return (
                    <tr style={{ cursor: "pointer" }}>
                      <td
                        onClick={() => handleUserClicked(item)}
                      >{`#${item?.id}`}</td>
                      <td onClick={() => handleUserClicked(item)}>
                        {item?.username}
                      </td>
                      <td onClick={() => handleUserClicked(item)}>
                        {item?.email ? item?.email : "-"}
                      </td>
                      <td onClick={() => handleUserClicked(item)}>
                        {" "}
                        <span
                          className={
                            item?.permissions === "Uploader"
                              ? "yellow-status"
                              : item?.permissions === "Admin"
                                ? "purple-status"
                                : item?.permissions === "Editor" && "green-status"
                          }
                        >
                          {item?.permissions ? item?.permissions : "-"}
                        </span>{" "}
                      </td>
                      <td>
                        <span
                          onClick={() => handleSettingClick(item)}
                          className="blue-color-eyes"
                        >
                          <AiOutlineSetting style={{ color: "#404040" }}>
                          </AiOutlineSetting>
                          {openDropDown == item?.id && (
                            <div
                              ref={wrapperRef}
                              className="user-setting-popup"
                            >
                              <p onClick={() => handleEditClicked(item)}>
                                Edit User
                              </p>
                              <p onClick={() => handleResetPassword(item)}>
                                Reset Password
                              </p>
                              <p onClick={() => handleEditClicked(item)}>
                                Change Permission
                              </p>
                              <p
                                onClick={() => {
                                  setDeleteModal(true);
                                  setModalOpen(true);
                                  setDeleteUserData(item);
                                }}
                              >
                                Remove User
                              </p>
                            </div>
                          )}
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <p style={{ textAlign: 'center' }}>{searchError}</p>
          </div>
          <div className="pagination-wrapper">
            <div className="entries-content">
              <span>{`Showing 1 to ${userData?.length} of ${totalCount} entries`}</span>
            </div>
            <div className="pagination">
              <Pagination
                total={totalPages}
                current={currentPage}
                onPageChange={(page) => handlePageChange(page)}
              />
            </div>
            <div className='per-pagination-wrapper'>
              <div onClick={() => setToggleDropDown(!toggleDropDown)} className={toggleDropDown ? 'selected-page open' : 'selected-page '}>
                <p>{perPage}</p>
                <div className='up-down-arrow'>
                  <span className='down-arrow'><BsChevronDown /></span>
                  <span className='up-arrow'><BsChevronUp /></span>
                </div>
              </div>
              {toggleDropDown && <ul className='per-page-dropdown'>
                {perPageValue?.map((item) => (
                  <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                ))}
              </ul>}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

/**
 * This function manage SaveEditData in redux store
 */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ SaveEditData }, dispatch),
  };
}

const mapStateToProps = (state) => ({
  data: state.homeReducer.data,
  adminData: state.homeReducer.adminData,
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);
