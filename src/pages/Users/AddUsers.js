import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import './users.style.scss'
import Layout from '../../Layout';
import DashboardApi from "../../services/DashboardApi";
import { Link, useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import validator from 'validator'
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import './App.css';
import MyGoogleMap from '../../components/MyGoogleMap';
import { GrFormClose } from 'react-icons/gr';

const options = [
    { value: 'Admin', label: 'Admin' },
    { value: 'Editor', label: 'Editor' },
    { value: 'Uploader', label: 'Uploader' }
]

/**
 * This function create new user record
 * This function collect user details and send json data to API 
*/
function AddUsers(props) {
    let history = useHistory();
    const [permission, setPermission] = useState({label: "Uploader", value: "Uploader"});
    const [name, setName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [emailAddress, setEmailAddress] = useState("");
    const [error, setError] = useState(false);
    const [userNameError, setUserNameError] = useState(undefined);
    const [mobileError, setMobileError] = useState(undefined);
    const [emailError, setEmailError] = useState(undefined);
    const [apiError, setApiError] = useState(undefined);
    const [modalOpen, setModalOpen] = useState(false);
    const [loader, setLoader] = useState(false);
    const [lat, setLat] = useState("31.771959");
    const [lng, setLng] = useState("35.217018");
    const [userRole, setUserRole] = useState(undefined);
    const [mapError, setMapError] = useState(true);

    /** 
     * this style is use for the selection fild
     */
    const customSelectStyles = {
        option: (provided, state) => ({
            ...provided,
            color: state.isSelected ? '#fff' : '#404040',
            padding: 10,
            borderRadius: 10,
            width: "98%",
            margin: '10px auto',
            cursor: 'pointer'
        }),
        control: (styles) => ({ ...styles, border: 'solid 1px #E5E5E5', backgroundColor: 'white', maxHeight: 46, minHeight: 46, fontSize: 14, borderRadius: 10, color: '#929292', fontWeight: '600' }),
    }

    /**
     * This function collect user details in form data in json formant
     * And send to API and get positive and error response from API
    */
    const handleSubmit = async() => {
        setLoader(true)
        if(!userNameError && !mobileError && !emailError) {
             if(props?.match?.params?.slug){
                let formData = {};
                formData["new_username"] = name;
                formData["new_mobile_no"] = mobileNumber;
                formData["new_permission"] = permission?.value;
                formData["user_role"] = userRole;
                formData["user_latitude"] = lat;
                formData["user_longitude"] = lng;
                DashboardApi.UserEdit(localStorage.getItem("neswell_access_token"),props?.match?.params?.slug,formData).then(
                    (json) => {               
                            if(json?.header?.status === 200 || json?.header?.status === 201){
                                setModalOpen(true)
                                history.push("/users")
                                setLoader(false)
                            } else {
                                setApiError(json.data.error)
                                setLoader(false)
                            }
                    }
                )
            } else {
                let formData = {};
                formData["email"] = emailAddress;
                formData["username"] = name;
                formData["contact_no"] = mobileNumber;
                formData["permission"] = permission?.value;
                formData["user_role"] = userRole;
                formData["user_latitude"] = lat;
                formData["user_longitude"] = lng;
                DashboardApi.AddUser(formData, localStorage.getItem("neswell_access_token")).then(
                    (json) => {                
                            if(json?.header?.status === 200 || json?.header?.status === 201 ){
                                history.push("/users")
                                setLoader(false)
                            } else {
                                setApiError(json?.data?.error) 
                                setLoader(false)  
                            }
                    }
                )
            }
        } else {
            
            setError(true)
            setLoader(false)
        }
    }

    /**
     * This function manage user edit feature and API set particular user data in edit from
     * And user change the details and that data send in json formant to API
    */
    useEffect(() => {
        setLoader(true)
        if(props?.match?.params?.slug){
            DashboardApi.UserDetails(localStorage.getItem("neswell_access_token"), props?.match?.params?.slug).then(
                (json) => {
                    if(!json?.data?.error){
                        setLat(json?.data?.user_latitude)
                        setName(json?.data?.username)
                        setLng(json?.data?.user_longitude)
                        setMobileNumber(json?.data?.mobile_No)
                        setEmailAddress(json?.data?.email)
                        setPermission({label: json?.data?.permissions.toString(), value: json?.data?.permissions.toString()})
                        setUserRole(json?.data?.user_role)
                        setLoader(false)
                    } else {
                        setLoader(false)
                    }
                }
            )
        } else {
            setLoader(false)
        }
    }, [props?.match?.params?.slug])

    /**
     * This function manage valid user name, user email, and user mobile number 
     * And if it's not proper, it's show error messages
    */
    useEffect(() => {
        props?.match?.params?.slug ? setUserNameError(false) : setUserNameError(true) 
        props?.match?.params?.slug ? setMobileError(false) : setMobileError(true)
        props?.match?.params?.slug ? setEmailError(false) : setEmailError(true)
    },[])

    /** 
     * This function is called onChange event of the username and check every time if the enter string is perfect based on function
     * The function validator library for the check entered data is validated or not
     */
    const handleChangeUserName = async(item) => {
        setName(item)
        if(validator.isEmpty(item)){
            await setUserNameError(true)
        } else {
            if (!validator.matches(item, "^[a-zA-Z0-9\.\-\@\+\_]*$")) {
                await setUserNameError(true)
            } else {
                await setUserNameError(false)
            }
        }
        if(item?.length > 60){
            setUserNameError(true)
        }
    }

    /**
     *  This function also call on every change of the phonenumber and check for the phone number validation 
    */
    const handleChangePhoneNumber = async(item) => {
        setMobileNumber(item)
        if(item?.length.toString() === "10"){
            if (!validator.matches(item, "^[0-9]*$")) {
                await setMobileError(true)
              } else {
                await setMobileError(false)
              }
        } else {
            await setMobileError(true)
        }

    }

    /**
     *  This function manage email id validation 
    */
    const handleEmailChange = async(item) => {
        setEmailAddress(item)
        if(item !== ""){
            if(validator.isEmail (item)){
                await setEmailError(false)
            } else {
                await setEmailError(true)
            }
        } else {
            await setEmailError(true)
        }
    }

    /**
     *This function manage map location 
    */
    const _onClick = (value) => {
        setMapError(false)
        setLat(value.lat)
        setLng(value.lng)
    }

    return (
        <Layout>
             <Modal 
                open={modalOpen} 
                closeModal={() => setModalOpen(false)}
                DeleteUsersRecord={() => {
                    setModalOpen(false)
                    history.push("/users")
                }}
                title={"Are you sure?"}
                subTitle={"changes have been saved successfully!"}
            />
            {loader && <Loader />}
            <div className="add-users-details-wrapper">
                <div className="users-title create-extract-header">
                    <h2>{props?.match?.params?.slug ? "Edit User" : "Add New User"}</h2>
                   <div className="create-extract-header">
                        <Link>
                            {" "}
                            <GrFormClose
                            onClick={() =>
                                history.push(`/users`)
                            }
                            />{" "}
                        </Link>
                   </div>
                </div>
                <div className="add-user-form-wrapper">
                    <div className="add-user-form">
                        <div className="form-group">
                            <span>User Name</span>
                            <input 
                                value={name} 
                                onChange={async(e) => {
                                    handleChangeUserName(e.target.value)
                                }
                                }
                                className="add-user-input" 
                                type="text" 
                                placeholder="User Name" 
                            />
                            {error && userNameError && (!name ? <p style={{color: "red"}}>Please enter a valid username</p> : <p style={{color: "red"}}>Enter a valid username. Should not allow white space. username must be less then 60 characters. This value may contain only letters, numbers, and @/./+/-/_ characters.</p>)}
                        </div>
                        <div className="form-group">
                            <span>Mobile Number</span>
                            <input 
                                value={mobileNumber} 
                                onChange={(e) => handleChangePhoneNumber(e.target.value)} 
                                className="add-user-input" 
                                type="number" 
                                placeholder="Mobile Number" 
                            />
                            {error && mobileError && <p style={{color: "red"}}>Mobile Number must be 10 Digit</p>}
                        </div>
                        <div className="form-group">
                            <span>Email ID</span>
                            <input 
                                style={{backgroundColor: props?.match?.params?.slug && "lightGray"}}
                                disabled={props?.match?.params?.slug ? "disabled" : ""} 
                                value={emailAddress} 
                                onChange={(e) => handleEmailChange(e.target.value)} 
                                className="add-user-input" 
                                type="text" 
                                placeholder="Enter Email Address" 
                            />
                            {error && emailError && <p style={{color: "red"}}>Please enter a valid email</p>}
                        </div>
                        <div className="form-group">
                            <span>Permission</span>
                            <Select 
                                value={permission}
                                styles={customSelectStyles}
                                options={options} 
                                classNamePrefix="select"
                                onChange={(value) => setPermission(value)}
                                isDisabled={props?.adminData?.email == props?.data?.email && true}
                            />
                        </div>
                        <div className="add-user-map">
                            <div className="form-group">
                                <span>Location</span>
                                {props?.match?.params?.slug ? (lat && lng && 
                                    <div className="google-map" style={{height: 350, width: 350}}>
                                        <MyGoogleMap  
                                            onClickMap={(value) => _onClick(value)}
                                            lat={lat}
                                            lng={lng}
                                            edit={true}
                                        />
                                    </div>) : 
                                    <div className="google-map" style={{height: 350, width: "100%"}}>
                                        <MyGoogleMap  
                                            onClickMap={(value) => _onClick(value)}
                                            lat={lat}
                                            lng={lng}
                                            edit={false}
                                        />
                                    </div>
                                }
                            </div>
                        </div>
                        <div className="form-group">
                            <span>User Role</span>
                            <input 
                                value={userRole} 
                                onChange={async(e) => {
                                    setUserRole(e.target.value)
                                }
                                }
                                className="add-user-input" 
                                type="text" 
                                placeholder="User Name" 
                            />
                        </div>
                    </div>
                    <div onClick={() => handleSubmit()} className="form-submit-btn">
                        {apiError && <p className="error-massage" >{apiError}</p>}
                        <button className="apply-filter primary-btn">{props?.match?.params?.slug ? "Update" : "Create"}</button>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

const mapStateToProps = (state) => ({
    data: state.homeReducer.data,
    adminData: state.homeReducer.adminData
  });

export default connect(mapStateToProps, null)(AddUsers)

