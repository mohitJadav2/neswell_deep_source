import React, { useState, useEffect } from "react";
import "./extractprofile.style.scss";
import { RiDeleteBinLine } from "react-icons/ri";
import { BiEdit } from "react-icons/bi";
import Layout from "../../Layout";
import { Link, useLocation, useHistory } from "react-router-dom";
import PDFimg from "../../assets/pdf.png";
import DashboardApi from "../../services/DashboardApi";
import Loader from "../../components/loader";
import MyGoogleMap from "../../components/MyGoogleMap";
import Modal from "../../components/ModalComponent";
import PageNotFound from "../PageNotFound/pageNotFound";
import JPEGImage from "../../assets/jpeg.png";
import JPGImage from "../../assets/jpg.png";
import PNGImage from "../../assets/png.png";



function ExtractViewProfile(props) {
  const location = useLocation();
  const [loader, setLoader] = useState(false);
  const [extractData, setExtractData] = useState(undefined);
  let history = useHistory();
  const [modalOpen, setModalOpen] = useState(false);
  const [sampleAddress, setSampleAddress] = useState(undefined);
  const [extractAddress, setExtractAddres] = useState(undefined);
  const [supplierAddress, setSupplierAddress] = useState(undefined);
  const [dataNotFound, setDataNotFound] = useState(false);
  const [samplelat, setSamplelat] = useState("");
  const [samplelong, setSamplelong] = useState("");
  const [extractlat, setExtractlat] = useState("");
  const [extractlong, setExtractlong] = useState("");
  const [supplierlat, setSupplierlat] = useState("");
  const [supplierlong, setSupplierlong] = useState("");
  const [color, setColor] = useState(undefined);
  const id = props?.match?.params?.slug;
  
  /**
   * This function fetch Address for supplier, sample, extract inventory from API
   */
  const fetchAddress = (lat, lng) => {
    return DashboardApi.fetchLocationAddress(
      lat,
      lng,
      localStorage.getItem("neswell_access_token")
    )
      .then((json) => {
        if (!json?.data?.error) {
          return json?.data?.message;
        } else {
          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  };
  /**
  * This function manage specific extract records from API and display in UI 
  */
  useEffect(() => {
    setLoader(true);
    DashboardApi.extractDetail(id, localStorage.getItem("neswell_access_token"))
      .then(async (json) => {
        if (!json?.data?.error) {
          setColor(JSON.parse(json?.data?.color_code).hex);
          setExtractData(json?.data);
          if (json?.data?.sample_inventory_location) {
            let [lat, long] = json?.data?.sample_inventory_location.split(",");
            setSamplelat(lat);
            setSamplelong(long);
            let sampleAddress = await fetchAddress(lat, long);
            setSampleAddress(sampleAddress);
          }
          if (json?.data?.extract_inventory_location) {
            let [lat, long] = json?.data?.extract_inventory_location.split(",");
            setExtractlat(lat);
            setExtractlong(long);
            let extractAddress = await fetchAddress(lat, long);
            setExtractAddres(extractAddress);
          }
          if (json?.data?.supplier_inventory_location) {
            let [lat, long] =
              json?.data?.supplier_inventory_location.split(",");
            setSupplierlat(long);
            setSupplierlong(lat);
            let supplierAddress = await fetchAddress(long, lat);
            setSupplierAddress(supplierAddress);
          }
          setLoader(false);
        } else {
          setLoader(false);
        }
      })
      .catch((e) => {
        setDataNotFound(true);
        setLoader(false);
      });
  }, []);
  /**
   * this function redirect to edit extract
  */
  const moveToCreatPage = () => {
    history.push(`/create-extract/${id}`);
  };
  
 /**
   * When user clicked delete button ,this function manage extract delete feature 
   * The function called delete Api with extract's id 
   */
  const handleDeleteClicked = () => {
    setLoader(true);
    DashboardApi.extractDelete(
      id,
      localStorage.getItem("neswell_access_token")
    ).then((json) => {
      if (!json?.data?.error) {
        setLoader(false);
        history.push("/users-extract");
        setModalOpen(false);
      } else {
        setLoader(false);
        setModalOpen(false);
      }
    });
  };
  return (
    <>
      {dataNotFound ? (
        <PageNotFound />
      ) : (
        <Layout>
          {loader && <Loader />}
          <Modal
            open={modalOpen}
            closeModal={() => setModalOpen(false)}
            DeleteUsersRecord={() => {
              handleDeleteClicked();
            }}
            title={"Are you sure?"}
            subTitle={"Do you want to delete this record?"}
            delete={true}
          />
          <div className="add-users-details-wrapper">
            <div className="users-title create-new-extract-header">
              <h2>View Extract</h2>
              <div className="create-extract-header">
                <Link onClick={() => moveToCreatPage()}>
                  {" "}
                  <BiEdit />{" "}
                </Link>
                <Link onClick={() => setModalOpen(true)}>
                  {" "}
                  <RiDeleteBinLine />{" "}
                </Link>
              </div>
            </div>
            <div className="create-extract-bottom-tag">
              <h2>Information</h2>
            </div>
            <div className="extract-view-profile-wrapper">
              <div className="extract-view-profile-inner-wrapper">
                <div className="details-colum">
                  <ul>
                    <li>
                      <span>Extract Name</span>
                      <p>{extractData?.name}</p>
                    </li>
                    <li>
                      <span>Price Per Kilogram</span>
                      <p>{extractData?.price_per_kg} {extractData?.price_per_kg_lable?.toUpperCase()}</p>
                    </li>
                    <li>
                      <span>CBD %</span>
                      <p>{extractData?.CBD}</p>
                    </li>
                    <li>
                      <span>THC %</span>
                      <p>{extractData?.THC}</p>
                    </li>
                  </ul>
                </div>
                <div className="details-colum">
                  <ul>
                    <li>
                      <span>Reservation Period (Days)</span>
                      <p>{extractData?.reservation_period}</p>
                    </li>
                    <li>
                      <span>Color Code</span>
                      <p>
                        <span
                          className="experiment-color"
                          style={{
                            backgroundColor: color,
                            marginRight: 10,
                            marginLeft: 0,
                          }}
                        ></span>{" "}
                        {color}
                      </p>
                    </li>
                    <li>
                      <span>CoA (Certificate of Analysis)</span>
                      <p>
                      {" "}
                      {
                        !extractData?.CoA ?(
                          <p>-</p>
                        ):
                        (
                           extractData?.CoA?.split("ext_extracts/")[1]?.split(".")[1] == "jpeg" ? (
                            <img src={JPEGImage} alt={""} />
                          ) : extractData?.CoA?.split("ext_extracts/")[1]?.split(".")[1] == "png" ? (
                            <img src={PNGImage} alt={""} />
                          ) : extractData?.CoA?.split("ext_extracts/")[1]?.split(".")[1] == "jpg" ? (
                            <img src={JPGImage} alt={""} />
                          ) : (
                            <img src={PDFimg} alt={""} />
                          )
                        
                        )
                      }
                      
                      {
                        !extractData?.CoA ?(
                          <p>-</p>
                        ):
                        (<a href={"https://" + extractData?.CoA} target="_blank">
                        {extractData?.CoA?.split("ext_extracts/")[1]}
                        </a>)
                      }
                    </p>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="create-extract-bottom-tag">
                <h2>Inventory</h2>
              </div>
              <div className="samples-inventory-title">
                <h2>- Samples</h2>
              </div>
              <div className="extract-view-profile-inner-wrapper">
                <div className="details-colum">
                  <ul>
                    <li>
                      <span>Samples Inventory</span>
                      <p>{extractData?.samples_inventory} G</p>
                    </li>
                    <li>
                      <span>Sample used</span>
                      <p>{extractData?.sample_used} G</p>
                    </li>
                    <li>
                      <span>Total Samples</span>
                      <p>{extractData?.total_samples} G</p>
                    </li>
                  </ul>
                </div>
                <div className="details-colum">
                  <ul>
                    <li>
                      <span>Sample inventory location</span>
                      <p>{sampleAddress}</p>
                      <div
                        style={{ height: "112px", width: "100%" }}
                        className="google-map"
                      >
                        {samplelong && samplelat && (
                          <MyGoogleMap
                            lat={samplelat}
                            lng={samplelong}
                          />
                        )}
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="samples-inventory-title extract-view-tag-border">
                <h2>- Extract</h2>
              </div>
              <div className="extract-view-profile-inner-wrapper">
                <div className="details-colum">
                  <ul>
                    <li>
                      <span>Extract Inventory</span>
                      <p>{extractData?.extract_inventory} Kg</p>
                    </li>
                    <li>
                      <span>Extract Inventory used</span>
                      <p>{extractData?.extract_inventory_used} Kg</p>
                    </li>
                    <li>
                      <span>Total Inventory</span>
                      <p>{extractData?.total_inventory} Kg</p>
                    </li>
                  </ul>
                </div>
                <div className="details-colum">
                  <ul>
                    <li>
                      <span>Extract inventory location</span>
                      <p>{extractAddress}</p>
                      <div
                        style={{ height: "112px", width: "100%" }}
                        className="google-map"
                      >
                        {extractlong && extractlat && (
                          <MyGoogleMap
                            lat={extractlat}
                            lng={extractlong}
                          />
                        )}
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="samples-inventory-title extract-view-tag-border">
                <h2>- Supplier</h2>
              </div>
              <div className="extract-view-profile-inner-wrapper">
                <div className="details-colum">
                  <ul>
                    <li>
                      <span> Supplier Company Name</span>
                      <p>{extractData?.supplier_id?.label}</p>
                    </li>
                    <li>
                      <span>Supplier Inventory</span>
                      <p>{extractData?.supplier_inventory} Kg</p>
                    </li>
                    <li>
                      <span>Supplier Inventory Availability</span>
                      <p>{extractData?.supplier_inventory_availability}</p>
                    </li>
                  </ul>
                </div>
                <div className="details-colum">
                  <ul>
                    <li>
                      <span>Supplier inventory location</span>
                      <p>{supplierAddress}</p>
                      <div
                        style={{ height: "112px", width: "100%" }}
                        className="google-map"
                      >
                        {supplierlat && supplierlong && (
                          <MyGoogleMap
                            lat={supplierlat}
                            lng={supplierlong}
                          />
                        )}
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      )}
    </>
  );
}

export default ExtractViewProfile;
