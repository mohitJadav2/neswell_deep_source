import React, { useState, useEffect } from "react";
import { Link,useHistory } from "react-router-dom";
import "./extractprofile.style.scss";
import { FiSearch } from "react-icons/fi";
import { RiDeleteBin5Fill,RiDeleteBin5Line } from "react-icons/ri";
import { FaFilter } from "react-icons/fa";
import Layout from "../../Layout";
import Pagination from "react-responsive-pagination";
import DashboardApi from "../../services/DashboardApi";
import Loader from "../../components/loader";
import Modal from "../../components/ModalComponent";
import Select from "react-select";
import {
  AiOutlineEye,
  AiFillCaretDown,
  AiFillCaretUp,
} from "react-icons/ai";
import { changeFilterStatus } from "../../Actions/HomeAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { BsChevronDown, BsChevronUp, BsPlusCircle, BsChevronLeft} from "react-icons/bs";

function UserExtract(props) {
  let history = useHistory();
  const [userData, setUserData] = useState(undefined);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalCount, setTotalCount] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [perPage, setPerPage] = useState(10);
  const [orderType, setOrderType] = useState("desc");
  const [orderField, setOrderField] = useState("id");
  const [searchText, setSearchText] = useState(undefined);
  const [filterHide, setFilterHide] = useState(true);
  const [arrayOfFilter, setArrayOfFilter] = useState([1]);
  const [searchError, setSearchError] = useState(undefined);
  const [filters, setFilters] = useState([
    {
      id: 1,
      columnValue: "",
      equalsValue: "",
      value: "",
    },
  ]);
  const [resetClicked, setResetClicked] = useState(false);
  const [filterError, setFilterError] = useState(false);
  const [toggleDropDown, setToggleDropDown] = useState(false);
  const perPageValue = [10, 25, 50, 100];

  /** 
   * this style is use for the selection fild
  */
  const customSelectStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? "#fff" : "#404040",
      padding: 10,
      borderRadius: 10,
      width: "90%",
      margin: "10px auto",
      boxSizing: "border-box",
    }),
    control: (styles) => ({
      ...styles,
      border: "solid 1px #E5E5E5",
      backgroundColor: "white",
      maxHeight: 46,
      minHeight: 46,
      fontSize: 14,
      borderRadius: 10,
      color: "#929292",
      fontWeight: "600",
    }),
  };

  const filterFileds = [
    { value: "id", label: "id" },
    { value: "name", label: "name" },
    { value: "profile_name", label: "profile_name" },
    { value: "CBD", label: "CBD" },
    { value: "THC", label: "THC" },
    { value: "price_per_kg", label: "price_per_kg" },
    { value: "reservation_period", label: "reservation_period" },
    { value: "sample_inventory", label: "sample_inventory" },
    { value: "sample_used", label: "sample_used" },
    { value: "sample_inventory_location", label: "sample_inventory_location" },
    { value: "extract_inventory", label: "extract_inventory" },
    { value: "extract_inventory_used", label: "extract_inventory_used" },
    { value: "supplier_inventory", label: "supplier_inventory" },
  ];

  const consitionOperater = [
    { value: "__icontains", label: "like" },
    { value: "__iexact", label: "equals" },
    { value: "__gt", label: ">" },
    { value: "__lt", label: "<" },
    { value: "__gte", label: ">=" },
    { value: "__lte", label: "<=" },
  ];

  /**
   *This function fetch extract data from API using admin token as parameter 
  */
  const handleFetchData = async () => {
    setLoader(true);
    let urlString = "";
    await filters?.map((item) => {
      let updatedUrl = urlString;
      urlString =
        updatedUrl +
        "&" +
        item.columnValue.value +
        item.equalsValue.value +
        "=" +
        item.value;
    });
    /**in this function use for search api and we set data in userData  */
    if (searchText) {
      DashboardApi.extractSearch(
        urlString.includes("undefined") ? "false" : "True",
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPage,
        orderType,
        orderField,
        currentPage,
        urlString.includes("undefined") ? "" : urlString
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalCount(json?.data?.count);
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      DashboardApi.extractProfile(
        urlString.includes("undefined") ? "false" : "True",
        localStorage.getItem("neswell_access_token"),
        currentPage,
        perPage,
        orderType,
        orderField,
        urlString.includes("undefined") ? "" : urlString
      )
        .then((json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPage;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setUserData(json?.data?.results);
            setLoader(false);
          } else {
            setLoader(false);
          }
        })
        .catch((e) => {
          setLoader(false);
        });
    }
  };

  useEffect(() => {
    handleFetchData();
  }, []);

  /**
   * This function manages pagination with passing page number in API
   */
  async function handlePageChange(page) {
    setLoader(true);
    setCurrentPage(page);
    let urlString = "";
    await filters?.map((item) => {
      let updatedUrl = urlString;
      urlString =
        updatedUrl +
        "&" +
        item.columnValue.value +
        item.equalsValue.value +
        "=" +
        item.value;
    });
    if (searchText) {
      DashboardApi.extractSearch(
        urlString.includes("undefined") ? "false" : "True",
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPage,
        orderType,
        orderField,
        page,
        urlString.includes("undefined") ? "" : urlString
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      DashboardApi.extractProfilePagination(
        urlString.includes("undefined") ? "false" : "True",
        localStorage.getItem("neswell_access_token"),
        page,
        perPage,
        orderType,
        orderField,
        urlString.includes("undefined") ? "" : urlString
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  }

  /**
   *When user clicked edit button, this fucntion redirect to extract edit page using extract id
  */
  const handelEditPage = async (item) => {
    history.push(`/view-extract/${item?.id}`);
  };

  
  /**
   * This function manage search feature 
   * This function passes user's enter value to API and get positive and error response
  */
  const handleSearch = async (searchText) => {
    setSearchText(searchText);
    setCurrentPage(1);
    let urlString = "";
    await filters?.map((item) => {
      let updatedUrl = urlString;
      urlString =
        updatedUrl +
        "&" +
        item.columnValue.value +
        item.equalsValue.value +
        "=" +
        item.value;
    });
    DashboardApi.extractSearch(
      urlString.includes("undefined") ? "false" : "True",
      searchText,
      localStorage.getItem("neswell_access_token"),
      perPage,
      orderType,
      orderField,
      1,
      urlString.includes("undefined") ? "" : urlString
    ).then((json) => {
      if (!json?.data?.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setUserData(json?.data?.results);
        if (json?.data?.results?.length == 0) {
          setSearchError("No data available");
        }else{
          setSearchError(undefined)
        }
      }
    });
  };

   /**
   * When user clicked delete button ,this function manage user delete feature 
   * The function called delete Api with user's id 
   */
  const handleDeleteClicked = () => {
    setModalOpen(false);
    setLoader(true);
    setLoader(true);
    DashboardApi.extractDelete(
      deleteUserData.id,
      localStorage.getItem("neswell_access_token")
    ).then((json) => {
      if (!json?.data?.error) {
        handleFetchData();
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  };

  /**
   * This function manages record display drop down 
   * When user change value of drop down at that time  this function call API with change value
  */
  const handlePerPageChanges = async (perPageNuumber) => {
    setLoader(true);
    let urlString = "";
    await filters?.map((item) => {
      let updatedUrl = urlString;
      urlString =
        updatedUrl +
        "&" +
        item.columnValue.value +
        item.equalsValue.value +
        "=" +
        item.value;
    });
    setToggleDropDown(false);
    setPerPage(perPageNuumber);
    if (searchText) {
      await DashboardApi.extractSearch(
        searchText,
        localStorage.getItem("neswell_access_token"),
        orderType,
        orderField,
        1
      ).then(async (json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPageNuumber;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      await DashboardApi.extractProfile(
        urlString.includes("undefined") ? "false" : "True",
        localStorage.getItem("neswell_access_token"),
        currentPage,
        perPageNuumber,
        orderType,
        orderField,
        urlString.includes("undefined") ? "" : urlString
      ).then(async (json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPageNuumber;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  };

  /**
   * This function manage user record column's ascending and descending order
  */
  const handleAscending = async (type) => {
    setLoader(true);
    setOrderType("asc");
    setOrderField(type);
    setCurrentPage(1);
    let urlString = "";
    await filters?.map((item) => {
      let updatedUrl = urlString;
      urlString =
        updatedUrl +
        "&" +
        item.columnValue.value +
        item.equalsValue.value +
        "=" +
        item.value;
    });
    if (searchText) {
      DashboardApi.extractSearch(
        urlString.includes("undefined") ? "false" : "True",
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPage,
        "asc",
        type,
        1,
        urlString.includes("undefined") ? "" : urlString
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      DashboardApi.extractProfile(
        urlString.includes("undefined") ? "false" : "True",
        localStorage.getItem("neswell_access_token"),
        1,
        perPage,
        "asc",
        type,
        urlString.includes("undefined") ? "" : urlString
      )
        .then((json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPage;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setUserData(json?.data?.results);
            setLoader(false);
          } else {
            setLoader(false);
          }
        })
        .catch((e) => {
          setLoader(false);
        });
    }
  };
  const handledescending = async (type) => {
    setLoader(true);
    setOrderType("desc");
    setOrderField(type);
    setCurrentPage(1);
    let urlString = "";
    await filters?.map((item) => {
      let updatedUrl = urlString;
      urlString =
        updatedUrl +
        "&" +
        item.columnValue.value +
        item.equalsValue.value +
        "=" +
        item.value;
    });
    if (searchText) {
      DashboardApi.extractSearch(
        urlString.includes("undefined") ? "false" : "True",
        searchText,
        localStorage.getItem("neswell_access_token"),
        perPage,
        "desc",
        type,
        1,
        urlString.includes("undefined") ? "" : urlString
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setUserData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      DashboardApi.extractProfile(
        urlString.includes("undefined") ? "false" : "True",
        localStorage.getItem("neswell_access_token"),
        1,
        perPage,
        "desc",
        type,
        urlString.includes("undefined") ? "" : urlString
      )
        .then((json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPage;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setUserData(json?.data?.results);
            setLoader(false);
          } else {
            setLoader(false);
          }
        })
        .catch((e) => {
          setLoader(false);
        });
    }
  };
  /**
   * This function manage extract filter records
  */
  const handelAddFilter = () => {
    setArrayOfFilter([
      ...arrayOfFilter,
      arrayOfFilter[arrayOfFilter.length - 1] + 1,
    ]);
    setFilters([
      ...filters,
      {
        id: filters[filters.length - 1].id + 1,
        columnValue: "",
        equalsValue: "",
        value: "",
      },
    ]);
  };
  const handleColumnSelect = (value, id, type) => {
    let newFilter = {
      id: id,
      columnValue: value,
      equalsValue: "__iexact",
      value: "20",
    };
    const isFilterPresent = filters.some((item) => item.id === id);
    if (isFilterPresent) {
      const updatedFilter = filters.map((item) => {
        if (item.id === id) {
          if (type == "column") {
            return { ...item, columnValue: value };
          }
          if (type == "equal") {
            return { ...item, equalsValue: value };
          }
          if (type == "value") {
            return { ...item, value: value };
          }
        }
        return item;
      });
      setFilters(updatedFilter);
    } else {
      setFilters((prevFilterState) => [...prevFilterState, newFilter]);
    }
  };
  /**
   * This function manage deleted filter records 
  */
  const handleDeleteClickedFileds = async (id) => {
    let updatedArray = await filters?.filter((item) => item?.id != id && item);
    setFilters(updatedArray);
  };
  /**
   * This function manages apply filter feature which pass filtered query string to API
   */
  const handleApplyFilter = async () => {
    setLoader(true);
    let urlString = "";
    let error = false;
    await filters?.map((item) => {
      if (
        item.columnValue.value &&
        item.equalsValue.value &&
        item.equalsValue.value
      ) {
        setFilterError(false);
      } else {
        error = true;
      }
      let updatedUrl = urlString;
      urlString =
        updatedUrl +
        "&" +
        item.columnValue.value +
        item.equalsValue.value +
        "=" +
        item.value;
    });
    if (!error) {
      setFilterError(false);
      DashboardApi.extractFilter(
        urlString,
        localStorage.getItem("neswell_access_token")
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalCount(json?.data?.count);
          setTotalPages(Math.ceil(totalPages));
          setUserData(json?.data?.results);
          setLoader(false);
          setFilterHide(true);
          handleFilturStatusChange(true);
          setOrderType("desc");
          setLoader(false);
        }
      });
    } else {
      if (filters.length == 1) {
        setLoader(true);
        DashboardApi.extractProfile(
          "false",
          localStorage.getItem("neswell_access_token"),
          currentPage,
          perPage,
          orderType,
          orderField,
          ""
        )
          .then((json) => {
            if (!json?.data?.error) {
              let totalPages = json?.data?.count / perPage;
              setTotalPages(Math.ceil(totalPages));
              setTotalCount(json?.data?.count);
              setUserData(json?.data?.results);
              setLoader(false);
              setFilterHide(true);
              handleFilturStatusChange(true);
              setOrderType("desc");
              setLoader(false);
            } else {
              setLoader(false);
            }
          })
          .catch((e) => {
            setLoader(false);
          });
      } else {
        setFilterError(true);
      }
    }
  };

  const handleFilturStatusChange = async (value) => {
    await props.changeFilterStatus(value);
  };
  /**
   *This function reset filter query to API 
  */
  const handelResetClick = () => {
    setResetClicked(true);
    setFilters([
      {
        id: 1,
        columnValue: "",
        equalsValue: "",
        value: "",
      },
    ]);
  };

  return (
    <Layout>
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => {
          handleDeleteClicked();
        }}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={deleteModal}
      />
      {!props?.extractFilterStatus ? (
        <div className="extract-profile-filter">
          <div className="extract-profile-filter-wrapper">
            <div className="title header">
              <h2>Query</h2>
              <Link
                onClick={() => {
                  setFilterHide(true);
                  handleFilturStatusChange(true);
                }}
              >
                {" "}
                <BsChevronLeft /> Back{" "}
              </Link>
            </div>
            {filters?.map((item, idx) => (
              <div className="filter-form-group-row">
                <div className="filter-form-group">
                  <Select
                    onChange={(value) =>
                      handleColumnSelect(value, item.id, "column")
                    }
                    value={item?.columnValue}
                    styles={customSelectStyles}
                    placeholder="-- Column --"
                    options={filterFileds}
                  />
                </div>
                <div className="filter-form-group">
                  <Select
                    onChange={(value) =>
                      handleColumnSelect(value, item.id, "equal")
                    }
                    value={item?.equalsValue}
                    styles={customSelectStyles}
                    placeholder="-- Equals --"
                    options={consitionOperater}
                  />
                </div>
                <div className="filter-form-group">
                  <input
                    onChange={(e) =>
                      handleColumnSelect(e.target.value, item.id, "value")
                    }
                    value={item?.value}
                    placeholder="-- Value --"
                    className="value-input"
                  />
                </div>
                {idx >= 1 && (
                  <div
                    onClick={() => handleDeleteClickedFileds(item.id)}
                    className="close-icon"
                  >
                    <Link>
                      {" "}
                      <RiDeleteBin5Fill />{" "}
                    </Link>
                  </div>
                )}
              </div>
            ))}
            <div>
              {filterError && (
                <p style={{ color: "red" }}>
                  Please fill all the details or delete extra fields
                </p>
              )}
            </div>
            <div className="filter-all-btn">
              <Link
                onClick={() => handelAddFilter()}
                className="add-filter primary-btn"
              >
                <BsPlusCircle /> Add rule
              </Link>
              <Link
                onClick={() => handelResetClick()}
                className="reset primary-btn"
              >
                Reset
              </Link>
              <Link
                onClick={() => handleApplyFilter()}
                className="apply-filter primary-btn"
              >
                Apply query
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="users-details-wrapper">
          {loader && <Loader />}
          <div className="users-details-inner-wrapper extract-profile-details-inner-wrapper">
            <div className="user-search">
              <div className="search">
                <button className="search-icon">
                  {" "}
                  <FiSearch />{" "}
                </button>
                <input
                  onChange={(e) => handleSearch(e.target.value)}
                  type="search"
                  placeholder="Search"
                  className="search-input"
                  value={searchText}
                />
              </div>
              <div className="add-user-btn extract-filter">
                <Link
                  onClick={() => {
                    setFilterHide(false);
                    handleFilturStatusChange(false);
                  }}
                  className="primary-btn"
                >
                  {" "}
                  <FaFilter /> Query
                </Link>
                <Link to="/create-extract" className="primary-btn">
                  {" "}
                  <BsPlusCircle /> Create New Extract
                </Link>
              </div>
            </div>
            <div className="user-details-table details-table">
              <table>
                <thead>
                  <tr>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("id")
                            : handleAscending("id");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Sr.
                        <div
                          style={{ marginLeft: 10, height: 22, lineHeight: 18 }}
                        >
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "id" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "id" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("name")
                            : handleAscending("name");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Profile Name
                        <div style={{ marginLeft: 10 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "name" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "name" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("extract_inventory")
                            : handleAscending("extract_inventory");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Inventory
                        <div style={{ marginLeft: 10 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "extract_inventory" &&
                                  orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "extract_inventory" &&
                                  orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("CBD")
                            : handleAscending("CBD");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        CBD %
                        <div style={{ marginLeft: 10 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "CBD" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "CBD" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("THC")
                            : handleAscending("THC");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        THC %
                        <div style={{ marginLeft: 10 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "THC" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "THC" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("price_per_kg")
                            : handleAscending("price_per_kg");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Price per Kg.
                        <div style={{ marginLeft: 10 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "price_per_kg" &&
                                  orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "price_per_kg" &&
                                  orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th style={{ cursor: "context-menu" }}>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {userData?.map((item, idx) => {
                    return (
                      <tr style={{ cursor: "pointer" }}>
                        <td
                          onClick={() => handelEditPage(item)}
                        >{`#${item?.id}`}</td>
                        <td onClick={() => handelEditPage(item)}>
                          {item?.name}
                        </td>
                        <td onClick={() => handelEditPage(item)}>
                          {item?.extract_inventory}
                        </td>
                        <td onClick={() => handelEditPage(item)}>
                          {item?.CBD}
                        </td>
                        <td onClick={() => handelEditPage(item)}>
                          {item?.THC}
                        </td>
                        <td onClick={() => handelEditPage(item)}>
                          {item?.price_per_kg}
                        </td>
                        <td>
                          <span
                            className="gray-color-eyes"
                            onClick={() => handelEditPage(item)}
                          >
                            <AiOutlineEye />
                          </span>{" "}
                          <span
                            className="delete-icon"
                            onClick={() => {
                              setDeleteModal(true);
                              setModalOpen(true);
                              setDeleteUserData(item);
                            }}
                          >
                            <RiDeleteBin5Line />
                          </span>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div>
              <p style={{ textAlign: "center" }}>{searchError}</p>
            </div>
            <div className="pagination-wrapper">
              <div className="entries-content">
                <span>{`Showing 1 to ${userData?.length} of ${totalCount} entries`}</span>
              </div>
              <div className="pagination">
                <Pagination
                  total={totalPages}
                  current={currentPage}
                  onPageChange={(page) => handlePageChange(page)}
                />
              </div>
              <div className="per-pagination-wrapper">
                <div
                  onClick={() => setToggleDropDown(!toggleDropDown)}
                  className={
                    toggleDropDown ? "selected-page open" : "selected-page "
                  }
                >
                  <p>{perPage}</p>
                  <div className="up-down-arrow">
                    <span className="down-arrow">
                      <BsChevronDown />
                    </span>
                    <span className="up-arrow">
                      <BsChevronUp />
                    </span>
                  </div>
                </div>
                {toggleDropDown && (
                  <ul className="per-page-dropdown">
                    {perPageValue?.map((item) => (
                      <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                    ))}
                  </ul>
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </Layout>
  );
}

/**
 * This function manage change FilterStatus in redux
 */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ changeFilterStatus }, dispatch),
  };
}

const mapStateToProps = (state) => ({
  extractFilterStatus: state.homeReducer.extractFilterStatus,
});

export default connect(mapStateToProps, mapDispatchToProps)(UserExtract);


