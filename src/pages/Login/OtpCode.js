import React, { useState, useEffect } from "react";
import Logo from "../../assets/images/Login/login-logo.svg";
import { FiMail, FiLock, FiChevronLeft } from "react-icons/fi";
import OtpInput from "react-otp-input";
import { useHistory } from "react-router-dom";
import LoginApi from "../../services/LoginApi";
import Loader from "../../components/loader";
import OtpTimer from 'otp-timer'

/**
 * In this function manages otp varification using API
 * The OTP receive from user's email address
 * When User enter the correct OTP page is redirect to new password page 
 * And if enter the wrong OTP it's redirect to forgot Password page
 */

function OTP() {
  let history = useHistory();
  const [otp, setOtp] = useState(undefined);
  const [error, setError] = useState(undefined);
  const handleChange = (otp) => setOtp(otp);
  const [loader, setLoader] = useState(false);
  const handleSubmit = () => {
    setLoader(true);
    let formData = {};
    formData["otp_data"] = otp?.toString();
    LoginApi.OtpVarification(formData, localStorage.getItem("email")).then(
      (json) => {
        if (!json.data.error) {
          setLoader(false);
          history.push("/reset-password");
        } else {
          setLoader(false);
          setError(json.data.error);
        }
      }
    );
  };

  /**
   * The useEffect() manage for forgot password tokenfrom api
   * Which manage tokens in local storage and decide where to redirect base on token
   */
  useEffect(() => {
    if(!localStorage.getItem("neswell_forgot_password_token")){
      history.push("/otp")
    }else if (localStorage.getItem("neswell_forgot_password_token") === "null") {
      history.push("/");
    }
    if(!localStorage.getItem("neswell_access_token")){
      history.push("/otp")
    }else if (localStorage.getItem("neswell_access_token") !== "null") {
      history.push("/users");
    }
  }, []);

  /**
   * This function use when the OTP time expire 
   * When the resend OTP button clicked then OTP send again the user email address 
   */
  const handleClick = () => {
    setLoader(true)
    let formData = {};
      formData["email"] = localStorage.getItem("email");
    LoginApi.ResetOtp(formData).then((json) => {
      if (!json?.data?.error) {
        setLoader(false)
      } else {
        setLoader(false);
        setError(json?.data?.error)
      }
    })
  }

  return (
    <div className="login-wrapper">
      {loader && <Loader />}
      <div className="login-inner-wrapper">
        <div className="login-left-bg"></div>
        <div className="login-right-fuild">
          <div>
            <div className="login-logo">
              <img src={Logo} alt="" />
            </div>
            <div className="logo-bottom-text">
              <h1>Enter 4 Digits Code</h1>
              <p className="forgot-password-text">
                Enter the 4 digits code that you <br />
                receive on your mail
              </p>
            </div>
            <div className="login-form">
              <div className="form-group">
                <OtpInput
                  value={otp}
                  onChange={handleChange}
                  numInputs={4}
                  shouldAutoFocus={true}
                  isInputNum={true}
                  containerStyle="otp-container"
                  inputStyle="otp-input"
                  onKeyDown={(event) => {event.key === 'Enter' && handleSubmit()}}
                />
              </div>
              <div>
                  <OtpTimer seconds= {60} minutes={0} resend={handleClick} />
              </div>
              {error && <p className="error-massage">{error}</p>}
              <div className="login-btn" onClick={() => handleSubmit()}>
                <button className="primary-btn">Continue</button>
              </div>
              <div className="forgot-text back-login-btn">
                <a onClick={() => history.push('/forgot-password')}>
                  {" "}
                  <FiChevronLeft /> Back to Forgot Password
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default OTP;
