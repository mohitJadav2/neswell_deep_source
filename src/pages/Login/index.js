import React, { useState, useEffect } from 'react';
import './Login.style.scss';
import Logo from '../../assets/images/Login/login-logo.svg';
import { FiMail, FiLock } from "react-icons/fi";
import { Link } from 'react-router-dom';
import LoginApi from "../../services/LoginApi";
import { useHistory } from "react-router-dom";
import validator from 'validator';
import Loader from "../../components/loader";
import DashboardApi from '../../services/DashboardApi';
import { SaveAdminData } from "../../Actions/HomeAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

/**we use functional component into the project */
/**
 * The Login function manage Login Api, handle errors which raises from api side or user interface side 
 */
function Login(props) {
    let history = useHistory();
    /**
     * we us useState react hook for for the storing value into the variable
    */
    const [userName, setUserName] = useState(undefined);
    const [password, setPassword] = useState(undefined);
    const [error, setError] = useState(undefined);
    const [emailError, setEmailError] = useState(undefined);
    const [passwordError, setPasswordError] = useState(undefined);
    const [submitError, setSubmitError] = useState(false);
    const [loader, setLoader] = useState(false);
    const [userDetails, setUserDetails] = useState(undefined);
    const emailInputRef = React.useRef(null);


    const handleSubmit = () => {
        /**
         * This function manages login api 
         * The User's submited form elements store in json formant and that json data passes in login api with POST method
         * After that based on the API's response the function navigate to the perticuler page 
         * And if the API retruns an error this function show the error massage
         */
        setLoader(true)
         setSubmitError(true)
        if(!emailError && password){
            setPasswordError(false)
            setEmailError(false)
            let formData = {};
            formData["email"] = userName;
            formData["password"] = password
            LoginApi.LoginForm(formData).then(
                (json) => {
                    if(json?.header?.status === 200){
                        localStorage.setItem("neswell_access_token", json.data.token)
                        DashboardApi.userProfile(json?.data?.token).then(
                            async(json1) => {
                                  if(!json1.data.error){
                                      await props?.SaveAdminData(json1?.data)
                                      setUserDetails(json?.data) 
                                      json1?.data?.permissions == "Admin" ? history.push("/users") : json1?.data?.permissions == "Editor" ? history.push("/users-extract") : history.push("/medical-indication-details")
                                     }
                                  setLoader(false)
                              }
                          )
                    } else {
                        setLoader(false)
                        setPassword("")
                        json?.data?.detail ? setError(json?.data?.detail) : setError(json?.data?.error)
                    }
                }
            ).catch(e => {
                setLoader(false)
                setPassword("")
            })
        } else {
            setLoader(false)
            if(!userName){
                setEmailError(true)
            }
            if(!password){
                setPasswordError(true)
                setPassword("")
            } else {
                setPasswordError(false)
            }
        }
        return () => clearTimeout(setLoader(false))
    }
    /**
     * This function handle the email and if email is invalid it's show the error message
     */
    const handleEmailChange = async(item) => {
        setError(undefined)
        setUserName(item)
        if(item !== ""){
            if(validator.isEmail (item)){
                await setEmailError(false)
            } else {
                await setEmailError(true)
            }
        } else {
            await setEmailError(true)
        }
    }
    /**
     * The useEffect() is use for store token in local storage
     */
    useEffect(() => {
        emailInputRef.current.focus();
        if(localStorage.getItem("neswell_access_token") != "null" && localStorage.getItem("neswell_access_token")){
            history.push('/users')
        }
        if(localStorage.getItem("neswell_access_token") == "null" && !localStorage.getItem("neswell_access_token")){
            history.push('/')
        }
    },[])

    return (
        <div className="login-wrapper">
            {loader && <Loader />}
            <div className="login-inner-wrapper">
                <div className="login-left-bg"></div>
                <div className="login-right-fuild">
                    <div>
                        <div className="login-logo">
                            <img src={Logo} alt="" />
                        </div>
                        <div className="logo-bottom-text">
                            <p>Welcome!</p>
                            <h1>Sign in to your account</h1>
                        </div>
                        <div className="login-form">
                            <div className="form-group">
                                <label> <FiMail /> </label>
                                <input 
                                    ref={emailInputRef}
                                    onChange={(e) => handleEmailChange(e.target.value)} 
                                    value={userName} 
                                    type="email" 
                                    placeholder="Email"
                                    onKeyDown={(event) => {event.key === 'Enter' && handleSubmit()}}
                                    autocomplete="off"
                                />
                                {emailError && submitError && (!userName ? <p style={{color: "red"}}>This field is required</p> : <p style={{color: "red"}}>Please enter a valid email address</p>)}
                            </div>
                            <div className="form-group">
                                <label> <FiLock /> </label>
                                <input 
                                    onChange={(e) => {
                                        setPassword(e.target.value)
                                        setError(undefined)
                                        setPasswordError(undefined)
                                    }}
                                    value={password} 
                                    type="password" 
                                    placeholder="Password" 
                                    onKeyDown={(event) => {event.key === 'Enter' && handleSubmit()}}
                                    autocomplete="off"
                                />
                                {passwordError && submitError && <p style={{color: "red"}}>Please enter a password</p>}
                            </div>
                            <div className="forgot-text">
                                <Link to='/forgot-password'>Forgot Password?</Link>
                            </div>
                                <div className="login-btn" onClick={() => handleSubmit()}>
                                    {error && <p className="error-massage" >{error}</p>}
                                    <button className="primary-btn">Login</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

/**
 * This function use for SaveAdminData in Redux store
 */
function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ SaveAdminData }, dispatch)
    }
  }
  
  const mapStateToProps = (state) => ({
    data: state.homeReducer.data
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(Login)
