import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./supplier.style.scss";
import Layout from "../../Layout";
import validator from "validator";
import SuppliersApi from "../../services/SuppliersApi";
import MyGoogleMap from "../../components/MyGoogleMap";
import DashboardApi from "../../services/DashboardApi";
import Loader from "../../components/loader";

function CreateSupplier(props) {
  let history = useHistory();
  const [permission, setPermission] = useState({
    label: "Uploader",
    value: "Uploader",
  });
  const [loader, setLoader] = useState(false);
  const [firmName, setFirmName] = useState("");
  const [firmNamError, setFirmNamError] = useState(undefined);
  const [contactName, setContactName] = useState("");
  const [phone, setPhone] = useState("");
  const [phoneError, setPhoneError] = useState(undefined);
  const [contactPosition, setContactPosition] = useState("");
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState(undefined);
  const [location, setLocation] = useState("");
  const [website, setWebSite] = useState("");
  const [supplierInventory, setSupplierInventory] = useState("");
  const [shippingTime, setShippingTime] = useState("");
  const [notes, setNotes] = useState("");
  const [apiError, setApiError] = useState(undefined);
  const [error, setError] = useState(false);
  const [supplierInventoryLatitude, setSupplierInventoryLatitude] =
    useState(props?.match?.params?.slug ? undefined : "31.771959");
  const [supplierInventoryLongitude, setSupplierInventoryLongitude] =
    useState(props?.match?.params?.slug ? undefined : "35.217018");
  const [CoAsAvailabilityRadioBtn, setCoAsAvailabilityRadioBtn] =
    useState(undefined);
  const [terpenesProfileRadioBtn, setTerpenesProfileRadioBtn] =
    useState(undefined);
  const [QADocumentRadioBtn, setQADocumentRadioBtn] = useState(undefined);
  const [preferredSupplierRadioBtn, setPreferredSupplierRadioBtn] =
    useState(undefined);
  const id = props?.match?.params?.slug;
  const [extractData, setExtractData] = useState(undefined);
  const [supplierAddress, setSupplierAddress] = useState(undefined);
  const [historyData, setHistoryData] = useState(undefined);

  /**
     * This function collect suppler details in form data in json formant
     * And send to API and get positive and error response from API
    */
  const handleSubmit = async () => {
    setLoader(true);
    if (firmName) {
      if (props?.match?.params?.slug) {
        setLoader(true);
        let formData = {};
        formData["supplier_Firm_name"] = firmName;
        formData["supplier_Contact_name"] = contactName;
        formData["supplier_Contact_Phone"] = phone;
        formData["supplier_Contact_Position_Role"] = contactPosition;
        formData["supplier_Contact_email"] = email;
        formData["supplier_Website"] = website;
        formData["supplier_CoAs_availability"] =
          CoAsAvailabilityRadioBtn === "true" ? true : false;
        formData["supplier_Terpenes_Profile"] =
          terpenesProfileRadioBtn === "true" ? true : false;
        formData["supplier_QA_Document"] =
          QADocumentRadioBtn === "true" ? true : false;
        formData["supplier_Storage_condition"] = "Notes done";
        formData["supplier_Shipping_time"] = shippingTime;
        formData["supplier_Notes"] = notes;
        formData["supplier_Is_Preferred_Supplier"] =
          preferredSupplierRadioBtn === "true" ? true : false;
        formData["supplier_Inventory_kg"] = supplierInventory;
        formData["supplier_Inventory_latitude"] = supplierInventoryLatitude;
        formData["supplier_Inventory_longitude"] = supplierInventoryLongitude;
        await SuppliersApi.SuppliersEdit(
          localStorage.getItem("neswell_access_token"),
          props?.match?.params?.slug,
          formData
        )
          .then((json) => {
            if (json?.header?.status === 200 || json?.header?.status === 201) {
              history.push("/suppliers-data");
              setLoader(false);
            } else {
              setApiError(json?.data.error);
              setLoader(false);
            }
          })
      } else {
        let formData = {};
        formData["supplier_Firm_name"] = firmName;
        formData["supplier_Contact_name"] = contactName;
        formData["supplier_Contact_Phone"] = phone;
        formData["supplier_Contact_Position_Role"] = contactPosition;
        formData["supplier_Contact_email"] = email;
        formData["supplier_Website"] = website;
        formData["supplier_CoAs_availability"] = CoAsAvailabilityRadioBtn;
        formData["supplier_Terpenes_Profile"] = terpenesProfileRadioBtn;
        formData["supplier_QA_Document"] = QADocumentRadioBtn;
        formData["supplier_Storage_condition"] = "Notes done";
        formData["supplier_Shipping_time"] = shippingTime;
        formData["supplier_Notes"] = notes;
        formData["supplier_Is_Preferred_Supplier"] = preferredSupplierRadioBtn;
        formData["supplier_Inventory_kg"] = supplierInventory;
        formData["supplier_Inventory_latitude"] = supplierInventoryLatitude;
        formData["supplier_Inventory_longitude"] = supplierInventoryLongitude;
        await SuppliersApi.AddSuppliers(
          formData,
          localStorage.getItem("neswell_access_token")
        ).then((json) => {
          if (json?.header?.status === 200 || json?.header?.status === 201) {
            history.push("/suppliers-data");
            setLoader(false);
          } else {
            setApiError(json?.data?.error);
            setLoader(false);
          }
        });
      }
    } else {
      setError(true);
      setLoader(false);
    }
  };
  /**
   * This function manage validation in supplier form
  */
  const handleChangeFirmName = async (item) => {
    setFirmName(item);
    if (validator.isEmpty(item)) {
      setFirmNamError(true);
    } else {
      if (!validator.matches(item, "^[a-zA-Z0-9.-@+_]*$")) {
        setFirmNamError(true);
      } else {
        setFirmNamError(false);
      }
    }
    if (item?.length > 60) {
      setFirmNamError(true);
    }
  };
  const handleChangeContactName = async (item) => {
    setContactName(item);
  };
  const handleChangePhone = async (item) => {
    setPhone(item);
    if (item?.length.toString() === "10") {
      if (!validator.matches(item, "^[0-9]*$")) {
        await setPhoneError(true);
      } else {
        await setPhoneError(false);
      }
    } else {
      await setPhoneError(true);
    }
  };
  const handleChangeContactPosition = async (item) => {
    setContactPosition(item);
  };
  const handleChangeEmail = async (item) => {
    setEmail(item);
    if (item !== "") {
      if (validator.isEmail(item)) {
        await setEmailError(false);
      } else {
        await setEmailError(true);
      }
    } else {
      await setEmailError(true);
    }
  };

  const handleWebSite = async (item) => {
    setWebSite(item);
  };
  const handleSupplierInventory = async (item) => {
    setSupplierInventory(item);
  };
  const handleShippingTime = async (item) => {
    setShippingTime(item);
  };
  const handleNotes = async (item) => {
    setNotes(item);
  };
  /**
   * This _onClick function manage Google map location 
   */
  const _onClick = (value) => {
    setSupplierInventoryLatitude(value.lat);
    setSupplierInventoryLongitude(value.lng);
  };
 /**This fucntion is use for when user go view page to edit page this api set already existence records in edit page. */
  useEffect(() => {
    setLoader(true);
    if (props?.match?.params?.slug) {
      SuppliersApi.SuppliersView(
        localStorage.getItem("neswell_access_token"),
        props?.match?.params?.slug
      ).then((json) => {
        if (!json?.data?.error) {
          setFirmName(json?.data?.supplier_Firm_name);
          setContactName(json?.data?.supplier_Contact_name);
          setPhone(json?.data?.supplier_Contact_Phone);
          setContactPosition(json?.data?.supplier_Contact_Position_Role);
          setEmail(json?.data?.supplier_Contact_email);
          setWebSite(json?.data?.supplier_Website);
          setCoAsAvailabilityRadioBtn(
            json?.data?.supplier_CoAs_availability?.toString()
          );
          setTerpenesProfileRadioBtn(
            json?.data?.supplier_Terpenes_Profile?.toString()
          );
          setQADocumentRadioBtn(json?.data?.supplier_QA_Document?.toString());
          setShippingTime(json?.data?.supplier_Shipping_time);
          setNotes(json?.data?.supplier_Notes);
          setPreferredSupplierRadioBtn(
            json?.data?.supplier_Is_Preferred_Supplier?.toString()
          );
          setSupplierInventory(json?.data?.supplier_Inventory_kg);
          setSupplierInventoryLatitude(json?.data?.supplier_Inventory_latitude);
          setSupplierInventoryLongitude(
            json?.data?.supplier_Inventory_longitude
          );
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      setLoader(false);
    }
  }, []);

  /**This function is use for fetch supplier's address from api */
  const fetchAddress = (lat, lng) => {
    return DashboardApi.fetchLocationAddress(
      lat,
      lng,
      localStorage.getItem("neswell_access_token")
    )
      .then((json) => {
        if (!json.data.error) {
          return json.data.message;
        } else {
          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  };
  /**This function is use for fatch address using latitude and longitude and set in map in edit page */
  useEffect(() => {
    setLoader(true);
    SuppliersApi.SuppliersView(localStorage.getItem("neswell_access_token"), id)
      .then(async (json) => {
        if (!json.data.error) {
          setExtractData(json?.data);

          let supplierAddress = await fetchAddress(
            json?.data?.supplier_Inventory_latitude,
            json?.data?.supplier_Inventory_longitude
          );
          setSupplierAddress(supplierAddress);
        } else {
          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  }, []);

  return (
    <Layout>
      {loader && <Loader />}
      <div className="add-users-details-wrapper view-supplier-details">
        <div className="users-title">
          <h2>Create Supplier</h2>
        </div>
        <div className="add-user-form-wrapper ">
          <div className="add-user-form">
            <div className="form-group">
              <span>Company Name</span>
              <input
                value={firmName}
                onChange={async (e) => {
                  handleChangeFirmName(e.target.value);
                }}
                className="add-user-input"
                type="text"
                placeholder="Enter Company Name"
                required="required"
              />
              {error &&
                firmNamError &&
                (!firmName ? (
                  <p style={{ color: "red" }}>Please enter a valid firmname</p>
                ) : (
                  <p style={{ color: "red" }}>Firmname is required. </p>
                ))}
            </div>
            <div className="form-group">
              <span>Contact Name</span>
              <input
                value={contactName}
                onChange={async (e) => {
                  handleChangeContactName(e.target.value);
                }}
                className="add-user-input"
                type="text"
                placeholder="Enter Contact Name"
                required="false"
              />
            </div>
            <div className="form-group">
              <span>Phone</span>
              <input
                value={phone}
                onChange={async (e) => {
                  handleChangePhone(e.target.value);
                }}
                className="add-user-input"
                type="number"
                placeholder="Enter Phone Number"
                required="false"
              />
            </div>
            <div className="form-group">
              <span>Contact Position</span>
              <input
                value={contactPosition}
                onChange={async (e) => {
                  handleChangeContactPosition(e.target.value);
                }}
                className="add-user-input"
                type="text"
                placeholder="Enter Contact Position"
                required="false"
              />
            </div>
            <div className="form-group">
              <span>Contact Email</span>
              <input
                value={email}
                onChange={async (e) => {
                  handleChangeEmail(e.target.value);
                }}
                className="add-user-input"
                type="email"
                placeholder="Enter Email Address"
                required="false"
              />
            </div>
            <div className="form-group">
              <span>Website</span>
              <input
                value={website}
                onChange={async (e) => {
                  handleWebSite(e.target.value);
                }}
                className="add-user-input"
                type="url"
                placeholder="Enter Website"
                required="false"
              />
            </div>
            <div className="form-group">
              <div className="availability-radio-group">
                <p>CoA's Availability</p>
                <div className="availability-radio">
                  <div className="radio">
                    <input
                      type="radio"
                      value={"true"}
                      checked={CoAsAvailabilityRadioBtn === "true"}
                      onChange={(e) =>
                        setCoAsAvailabilityRadioBtn(e.target.value)
                      }
                    />
                    <label for="CoAsAvailability" id="CoAsAvailability" className="radio-label">
                      Yes
                    </label>
                  </div>
                  <div className="radio">
                    <input
                      type="radio"
                      value={"false"}
                      checked={CoAsAvailabilityRadioBtn === "false"}
                      onChange={(e) =>
                        setCoAsAvailabilityRadioBtn(e.target.value)
                      }
                    />
                    <label for="CoAsAvailability" id="CoAsAvailability" className="radio-label">
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="availability-radio-group">
                <p>Terpenes Profile</p>
                <div className="availability-radio">
                  <div className="radio">
                    <input
                      type="radio"
                      value={"true"}
                      checked={terpenesProfileRadioBtn === "true"}
                      onChange={(e) =>
                        setTerpenesProfileRadioBtn(e.target.value)
                      }
                    />
                    <label for="terpenesProfile" id="terpenesProfile" className="radio-label">
                      Yes
                    </label>
                  </div>
                  <div className="radio">
                    <input
                      type="radio"
                      value={"false"}
                      checked={terpenesProfileRadioBtn === "false"}
                      onChange={(e) =>
                        setTerpenesProfileRadioBtn(e.target.value)
                      }
                    />
                    <label for="terpenesProfile" id="terpenesProfile" className="radio-label">
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="availability-radio-group">
                <p>QA Document</p>
                <div className="availability-radio">
                  <div className="radio">
                    <input
                      type="radio"
                      value={"true"}
                      checked={QADocumentRadioBtn === "true"}
                      onChange={(e) => setQADocumentRadioBtn(e.target.value)}
                    />
                    <label for="QADocument" className="radio-label">
                      Yes
                    </label>
                  </div>
                  <div className="radio">
                    <input
                      type="radio"
                      value={"false"}
                      checked={QADocumentRadioBtn === "false"}
                      onChange={(e) => setQADocumentRadioBtn(e.target.value)}
                    />
                    <label for="QADocument" className="radio-label">
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <span>Supplier Inventory (KG)</span>
              <input
                value={supplierInventory}
                onChange={async (e) => {
                  handleSupplierInventory(e.target.value);
                }}
                className="add-user-input"
                type="text"
                placeholder="Enter Supplier Inventory"
                required="false"
              />
            </div>
            <div className="form-group">
              <span>Shipping Time</span>
              <input
                value={shippingTime}
                onChange={async (e) => {
                  handleShippingTime(e.target.value);
                }}
                className="add-user-input"
                type="number"
                placeholder="Enter Days"
                required="false"
              />
            </div>
            <div className="form-group">
              <div className="availability-radio-group">
                <p>Is Preferred Supplier</p>
                <div className="availability-radio">
                  <div className="radio">
                    <input
                      type="radio"
                      value={"true"}
                      checked={preferredSupplierRadioBtn === "true"}
                      onChange={(e) =>
                        setPreferredSupplierRadioBtn(e.target.value)
                      }
                    />
                    <label for="preferredSupplier" className="radio-label">
                      Yes
                    </label>
                  </div>
                  <div className="radio">
                    <input
                      type="radio"
                      checked={preferredSupplierRadioBtn === "false"}
                      value={"false"}
                      onChange={(e) =>
                        setPreferredSupplierRadioBtn(e.target.value)
                      }
                    />
                    <label for="preferredSupplier" className="radio-label">
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div className="form-group">
              <span>Notes</span>
              <textarea
                style={{ height: "166px", width: "100%" }}
                value={notes}
                onChange={async (e) => {
                  handleNotes(e.target.value);
                }}
                className="add-user-input"
                type="textarea"
                placeholder="Note about  Supplier"
                required="false"
              />
            </div>
            <div className="form-group">
              <span>Inventory Location</span>
              <div
                style={{ height: "166px", width: "100%" }}
                className="google-map"
              >
                {(supplierInventoryLatitude && supplierInventoryLongitude) && <MyGoogleMap
                  viewProfile={false}
                  onClickMap={(value) => _onClick(value)}
                  lat={supplierInventoryLatitude}
                  lng={supplierInventoryLongitude}
                />}
              </div>
            </div>
          </div>
          <div onClick={() => handleSubmit()} className="form-submit-btn">
            <button className="success-btn">Create</button>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default CreateSupplier;
