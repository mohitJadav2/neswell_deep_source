import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import "./supplier.style.scss";
import { FiSearch } from "react-icons/fi";
import {
  AiOutlineSetting,
  AiFillCaretUp,
  AiFillCaretDown,
} from "react-icons/ai";
import { BsPlusCircle } from "react-icons/bs";
import Layout from "../../Layout";
import Pagination from "react-responsive-pagination";
import SuppliersApi from "../../services/SuppliersApi";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import { useHistory } from "react-router-dom";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";

function SuppliersData() {
  const [openDropDown, setOpenDropDown] = useState(undefined);
  const [SupplierData, setSupplierData] = useState(undefined);
  const [totalPages, setTotalPages] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [perPage, setPerPage] = useState(10);
  const [orderType, setOrderType] = useState("desc");
  const [orderField, setOrderField] = useState("id");
  const [searchText, setSearchText] = useState("");
  const [searchError, setSearchError] = useState(undefined)
  const [toggleDropDown, setToggleDropDown] = useState(false);
  let history = useHistory();
  const perPageValue = [10, 25, 50, 100];
  /**
   * This function for handling the action  pop for every user 
   */
  const handleSettingClick = async (item) => {
    openDropDown == item?.id
      ? setOpenDropDown(undefined)
      : setOpenDropDown(item?.id);
  };
  /**
     * When user clicked outside of the alert popup box this function executed 
     * The function manages mouse event
  */
  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenDropDown(undefined);
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  /**
   * This ref function manage the when user clicked outside the fucntion 
  */
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  useEffect(() => {
    setLoader(true)
    SuppliersApi.SuppliersData(
      orderType,
      orderField,
      localStorage.getItem("neswell_access_token"),
      page,
      perPage
    ).then((res) => {
      setSupplierData(res?.data?.results);
      let totalPages = res?.data?.count / perPage;
      setTotalPages(Math.ceil(totalPages));
      setTotalCount(res?.data?.count);
      setLoader(false)
    });
  }, []);

  /**
    * This function manages pagination with passing page number in API
   */
  function handlePageChange(page) {
    setLoader(true);
    setSearchText(searchText);
    setCurrentPage(page);
    SuppliersApi.SuppliersSearch(
      orderType, 
      orderField,
      localStorage.getItem("neswell_access_token"),
      searchText,
      page,
      perPage
    ).then((json) => {
      if (!json.data.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setSupplierData(json?.data?.results);
        setCurrentPage(page)
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }
   /**
     * This function manages record display drop down 
     * When user change value of drop down at that time  this function call API with change value
    */
  const handlePerPageChanges = async (perPageNuumber) => {
    setToggleDropDown(false);
    setPerPage(perPageNuumber);
    if (searchText) {
      await SuppliersApi.SuppliersSearch(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        String,
        1
      ).then(async (json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPageNuumber;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
        } 
      });
    } else {
      await SuppliersApi.SuppliersData(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        page,
        perPageNuumber
      ).then(async (json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPageNuumber;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
        } 
      });
    }
  };
 
   /**
     * This function manage search feature 
     * This function passes user's enter value to API and get positive and error response
    */
  const handleSearch = (searchText) => {
    setSearchText(searchText);
    setCurrentPage(page );
    SuppliersApi.SuppliersSearch(
      orderType,
      orderField,
      localStorage.getItem("neswell_access_token"),
      searchText,
      currentPage,
      perPage
    ).then((json) => {
      if (!json?.data?.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setSupplierData(json?.data?.results);
        setSearchText(searchText)
        setSearchError(json?.data?.message)
      }
    });
  };
  
 /**
     * When user clicked delete button ,this function manage user delete feature 
     * The function called delete Api with user's id 
  */
  const handeleDeleteClicked = () => {
    setModalOpen(false);
    setLoader(true);
    if (searchText) {
      SuppliersApi.SuppliersDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData?.id
      ).then((json) => {
        if (!json.data.error) {
          SuppliersApi.SuppliersSearch(
            orderType,
            orderField,
            localStorage.getItem("neswell_access_token"),
            searchText,
            currentPage,
            perPage
          ).then((json) => {
            if (!json.data.error) {
              let totalPages = json?.data?.count / perPage;
              setTotalPages(Math.ceil(totalPages));
              setSupplierData(json?.data?.results);
              setTotalCount(json?.data?.count);
              setLoader(false);
            } else {
              setLoader(false);
            }
          });
        } else {
          setLoader(false);
        }
      });
    } else {
      SuppliersApi.SuppliersDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData?.id
      ).then((json) => {
        if (!json?.data?.error) {
          SuppliersApi.SuppliersData(
            orderType,
            orderField,
            localStorage.getItem("neswell_access_token"),
            currentPage,
            perPage
          ).then((res) => {
            setSupplierData(res?.data?.results);
            let totalPages = res?.data?.count / perPage;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(res?.data?.count);
            setLoader(false);
          });
        } else {
          setLoader(false);
        }
      });
      
    }
  };
  /**
    * This function manage user record column's ascending and descending order
  */
  const handleAscending = (fieldName) => {
    setOrderType("aesc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      SuppliersApi.SuppliersData(
        "aesc",
        orderField,
        localStorage.getItem("neswell_access_token"),
        1,
        perPage
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setSupplierData(json?.data?.results);
          setTotalCount(json?.data?.count);
        } else {
          setLoader(false);
        }
      });
    } else {
      SuppliersApi.SuppliersData(
        "aesc",
        orderField,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  };
 
  const handledescending = (fieldName) => {
    setOrderType("desc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      SuppliersApi.SuppliersData(
        "desc",
        orderField,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setSupplierData(json?.data?.results);
          setTotalCount(json?.data?.count);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      SuppliersApi.SuppliersData(
        "desc",
        orderField,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  };

  /**
   * This function use for to redirect view Supplier's details 
  */
  const handleSupplierClicked = async (item) => {
    await SuppliersApi.SuppliersView;
    history.push(`/view-suppliers/${item?.id}`);
  };

/**
 * This function use for to redirect edit Supplier's records details 
 */
  const handleEditClicked = async (item) => {
    history.push(`create-suppliers/${item?.id}`);
  };
  return (
    <Layout>
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => {
          deleteModal ? handeleDeleteClicked() : setModalOpen(false);
        }}
        title={"Are you sure?"}
        subTitle={
          deleteModal
            ? "Do you want to delete this record?"
            : "we have emailed user password reset link!"
        }
        delete={deleteModal}
      />
      {loader && <Loader />}
      <div className="users-details-wrapper">
        <div className="users-details-inner-wrapper">
          <div className="user-search">
            <div className="search">
              <button className="search-icon">
                {" "}
                <FiSearch />{" "}
              </button>
              <input
                onChange={(e) => handleSearch(e.target.value)}
                type="search"
                placeholder="Search"
                className="search-input"
              />
            </div>
            <div className="add-user-btn">
              <Link to="/create-suppliers" className="primary-btn">
                {" "}
                <BsPlusCircle /> Create Suppliers
              </Link>
            </div>
          </div>
          <div className="user-details-table details-table">
            <table>
              <thead>
                <tr>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "aesc"
                          ? handledescending("supplier_Firm_name")
                          : handleAscending("supplier_Firm_name");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        cursor: "pointer",
                      }}
                    >
                      Firm Name
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField == "supplier_Firm_name" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField == "supplier_Firm_name" &&
                                orderType == "aesc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "aesc"
                          ? handledescending("supplier_Contact_name")
                          : handleAscending("supplier_Contact_name");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        
                      }}
                    >
                      Contact Name
                      <div style={{ marginLeft: 10 }}>
                        <div style={{cursor: "pointer",  marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField == "supplier_Contact_name" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{cursor: "pointer",  marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField == "supplier_Contact_name" &&
                                orderType == "aesc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "aesc"
                          ? handledescending("supplier_Contact_Phone")
                          : handleAscending("supplier_Contact_Phone");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        
                      }}
                    >
                      Phone
                      <div style={{ marginLeft: 10 }}>
                        <div style={{cursor: "pointer",  marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField == "supplier_Contact_Phone" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{cursor: "pointer",  marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField == "supplier_Contact_Phone" &&
                                orderType == "aesc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "aesc"
                          ? handledescending("supplier_Contact_Position_Role")
                          : handleAscending("supplier_Contact_Position_Role");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        
                      }}
                    >
                      Position
                      <div style={{ marginLeft: 10 }}>
                        <div style={{cursor: "pointer",  marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField ==
                                  "supplier_Contact_Position_Role" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{cursor: "pointer",  marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField ==
                                  "supplier_Contact_Position_Role" &&
                                orderType == "aesc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody style={{cursor: "pointer"}}>
                {SupplierData?.map((item, idx) => {
                  return (
                    <tr>
                      <td
                        onClick={() => handleSupplierClicked(item)}
                      >{`${item?.supplier_Firm_name}`}</td>
                      <td
                        onClick={() => handleSupplierClicked(item)}
                      >{`${item?.supplier_Contact_name}`}</td>
                      <td onClick={() => handleSupplierClicked(item)}>
                        {`${item?.supplier_Contact_Phone}`}{" "}
                      </td>
                      <td
                        onClick={() => handleSupplierClicked(item)}
                      >{`${item?.supplier_Contact_Position_Role}`}</td>
                      <td>
                        <span
                          onClick={() => handleSettingClick(item)}
                          className="blue-color-eyes"
                        >
                          <AiOutlineSetting style={{ color: "#404040" }} />
                          {openDropDown == item?.id && (
                            <div
                              ref={wrapperRef}
                              className="user-setting-popup"
                            >
                              <p onClick={() => handleEditClicked(item)}> Edit Supplier </p>
                              <p
                                onClick={() => {
                                  setDeleteModal(true);
                                  setModalOpen(true);
                                  setDeleteUserData(item);
                                }}
                              >
                                Remove User
                              </p>
                            </div>
                          )}
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <p style={{textAlign: 'center'}}>{searchError}</p>
          </div>
          <div className="pagination-wrapper">
            <div className="entries-content">
              <span>{`Showing 1 to ${SupplierData?.length} of ${totalCount} entries`}</span>
            </div>
            <div className="pagination">
              <Pagination
                total={totalPages}
                current={currentPage}
                onPageChange={(page) => handlePageChange(page)}
              />
            </div>
            <div className="per-pagination-wrapper">
              <div
                onClick={() => setToggleDropDown(!toggleDropDown)}
                className={
                  toggleDropDown ? "selected-page open" : "selected-page "
                }
              >
                <p>{perPage}</p>
                <div className="up-down-arrow">
                  <span className="down-arrow">
                    <BsChevronDown />
                  </span>
                  <span className="up-arrow">
                    <BsChevronUp />
                  </span>
                </div>
              </div>
              {toggleDropDown && (
                <ul className="per-page-dropdown">
                  {perPageValue?.map((item) => (
                    <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                  ))}
                </ul>
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default SuppliersData;
