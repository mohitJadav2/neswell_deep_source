import React, { useState, useEffect } from "react";
import "./supplier.style.scss";
import Layout from "../../Layout";
import SuppliersApi from "../../services/SuppliersApi";
import MyGoogleMap from "../../components/MyGoogleMap";
import DashboardApi from "../../services/DashboardApi";
import Loader from "../../components/loader";
import { Link } from "react-router-dom";
import { BiEdit } from "react-icons/bi";
import { RiDeleteBinLine } from "react-icons/ri";
import Modal from "../../components/ModalComponent";
import { useHistory } from "react-router-dom";
import { AiOutlineEye } from 'react-icons/ai';
const ViewSuppliers = (props) => {
  const [SupplierData, setSupplierData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [extract, setExtract] = useState(undefined);
  const [extractData, setExtractData] = useState(undefined);
  const [supplierAddress, setSupplierAddress] = useState(undefined);
  const [popUpModalOpen, setPopUpModalOpen] = useState(false);
  const id = props?.match?.params?.slug;
  let history = useHistory();

  /**
   * This function manage fetch supplier's address from API
  */
  const fetchAddress = (lat, lng) => {
    return DashboardApi.extractDetail(
      lat,
      lng,
      localStorage.getItem("neswell_access_token")
    )
      .then((json) => {
        if (!json.data.error) {
          return json.data.message;
        } else {

          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  };

  /**
   * This function manage Supplier's details form API
   */
  useEffect(() => {
    setLoader(true);
    SuppliersApi.SuppliersView(localStorage.getItem("neswell_access_token"), id)
      .then(async (json) => {
        if (!json.data.error) {
          setExtractData(json?.data);
          let supplierAddress = await fetchAddress(
            json?.data?.supplier_Inventory_latitude,
            json?.data?.supplier_Inventory_longitude
          );
          setExtract(json?.data?.extract_data)
          setSupplierAddress(supplierAddress);
        } else {

          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  }, []);
  /**
     * When user clicked delete button ,this function manage user delete feature 
     * The function called delete Api with user's id 
  */
  const handleDeleteClicked = () => {
    setPopUpModalOpen(false)
    setLoader(true);
    SuppliersApi.SuppliersDelete(
      localStorage.getItem("neswell_access_token"),
      id
    ).then((json) => {
      if (!json.data.error) {
        setLoader(false);
        history.push("/suppliers-data");
        
      } else {
        setLoader(false);
      }
    });
  };

  /**
    * This function manage specific suppliers records from API and display in UI 
  */
  useEffect(() => {
    setLoader(true);
    SuppliersApi.SuppliersView(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug
    ).then((json) => {
      if (!json?.data?.error) {
        setSupplierData(json?.data);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }, []);
  const handleExtraxtClicked = async (item) => {
    await  DashboardApi.extractDetail;
    history.push(`/view-extract/${item?.id}`);
  };

  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={popUpModalOpen}
        closeModal={() => setPopUpModalOpen(false)}
        DeleteUsersRecord={() => handleDeleteClicked()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={true}
      />
      <div className="add-users-details-wrapper view-supplier-details">
        <div className="users-title create-new-extract-header">
          <h2>View Suppliers</h2>
          <div className="create-extract-header">
            <Link to={`/create-suppliers/${props?.match?.params?.slug}`}>
              {" "}
              <BiEdit />{" "}
            </Link>
            <Link onClick={() => setPopUpModalOpen(true)}>
              {" "}
              <RiDeleteBinLine />{" "}
            </Link>
          </div>
        </div>
        <div className="create-extract-bottom-tag">
          <h2>Information</h2>
        </div>
        <div className="extract-view-profile-wrapper">
          <div className="extract-view-profile-inner-wrapper">
            <div className="details-colum">
              <ul>
                <li>
                  <span>Firm Name</span>
                  <p>{SupplierData?.supplier_Firm_name}</p>
                </li>
                <li>
                  <span>Contact Position</span>
                  <p>{SupplierData?.supplier_Contact_Position_Role}</p>
                </li>
                <li>
                  <span>Website</span>
                  <p>{SupplierData?.supplier_Website}</p>
                </li>
                <li>
                  <span>QA Document</span>
                  <p> {SupplierData?.supplier_QA_Document ? "yes" : "No"} </p>
                </li>
                <li>
                  <span>Is Preferred Supplier</span>
                  <p>
                    {" "}
                    {SupplierData?.supplier_Is_Preferred_Supplier
                      ? "yes"
                      : "No"}
                  </p>
                </li>
              </ul>
            </div>
            <div className="details-colum">
              <ul>
                <li>
                  <span>Contact Name</span>
                  <p>{SupplierData?.supplier_Contact_name}</p>
                </li>
                <li>
                  <span>Contact Email</span>
                  <p>{SupplierData?.supplier_Contact_email}</p>
                </li>
                <li>
                  <span>CoA's Availability</span>
                  <p>
                    {SupplierData?.supplier_CoAs_availability ? "yes" : "No"}
                  </p>
                </li>
                <li>
                  <span>Notes</span>
                  {!SupplierData?.supplier_Notes ? (<p>--</p>):(
                    <p>{SupplierData?.supplier_Notes} </p>
                  )}
                </li>
              </ul>
            </div>
            <div className="details-colum">
              <ul>
                <li>
                  <span>Phone</span>
                  <p>{SupplierData?.supplier_Contact_Phone}</p>
                </li>
                <li>
                  <span>Terpenes Profile</span>
                  <p>
                    {SupplierData?.supplier_Terpenes_Profile ? "yes" : "No"}
                  </p>
                </li>
                <li>
                  <span>Shipping Time</span>
                  <p> {SupplierData?.supplier_Shipping_time} </p>
                </li>
                <li>
                  <span>Inventory Location</span>
                  <p>{supplierAddress}</p>
                  <div
                    style={{ height: "112px", width: "100%" }}
                    className="google-map"
                  >
                   {(SupplierData?.supplier_Inventory_latitude && SupplierData?.supplier_Inventory_longitude) &&  <MyGoogleMap
                      viewProfile={true}
                      lat={SupplierData?.supplier_Inventory_latitude}
                      lng={SupplierData?.supplier_Inventory_longitude}
                    />}
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="create-extract-bottom-tag">
          <h2>Extract</h2>
        </div>
        <div className="user-details-table details-table">
          <table >
            <thead>
              <tr>
                <th>Sr.</th>
                <th>Name</th>
                <th>Supplier Inventory</th>
                <th>Supplier Inventory Availablity</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody style={{cursor: "pointer"}}>
            {extract?.map((item, idx) => {
                  return (
                    <tr>
                      <td  onClick={() => handleExtraxtClicked(item)}>{item?.id}</td>
                      <td onClick={() => handleExtraxtClicked(item)}>{item?.name}</td>
                      <td onClick={() => handleExtraxtClicked(item)}>{item?.supplier_inventory}</td>
                      <td onClick={() => handleExtraxtClicked(item)}>{item?.supplier_inventory_availability}</td>
                      <td>
                        <span
                          onClick={() => handleExtraxtClicked(item)}
                          className="gray-color-eyes"
                        >
                          <AiOutlineEye />
                        </span>{" "}
                     </td>
                    </tr>
                    
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default ViewSuppliers;
