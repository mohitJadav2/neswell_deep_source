import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import './medicalIndication.style.scss'
import { BiEdit } from "react-icons/bi";
import { MdContentCopy } from "react-icons/md";
import { AiOutlinePlusCircle, AiOutlineEye } from "react-icons/ai";
import Layout from '../../Layout';
import { Link } from 'react-router-dom';
import Pagination from 'react-responsive-pagination';


function MedicalIndicationDetals() {
    const totalPages = 100;

    const [currentPage, setCurrentPage] = useState(1);

    function handlePageChange(page) {
        setCurrentPage(page);
        // ... do something with `page`
    }

    return (
        <Layout>
            <div className="add-users-details-wrapper">
                <div className='medical-indication-details-view-header'>
                    <div className='medical-indication-view-wrapper'>
                        <div className='name-left-column'>
                            <p>Medical Indication: <strong>Indication Name #02</strong></p>
                        </div>
                        <div className='color-right-column'>
                            <p>Task Type: <strong>View</strong></p>
                            <ul>
                                <li> <span style={{ backgroundColor: '#FF8880', }} className='status-color'></span> 0-39 </li>
                                <li> <span style={{ backgroundColor: '#FFCA91', }} className='status-color'></span> 0-39 </li>
                                <li> <span style={{ backgroundColor: '#FFE18C', }} className='status-color'></span> 0-39 </li>
                                <li> <span style={{ backgroundColor: '#A4CD9D', }} className='status-color'></span> 0-39 </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="medical-indication-details-wrapper">
                    <div className='medical-indication-table'>
                        <div className='medical-indication-table-details medical-details-table medical-indication-details-view-table'>
                            <table>
                                <thead>
                                    <tr>
                                        <th style={{ textAlign: 'center' }} >F1</th>
                                        <th style={{ textAlign: 'center' }} >F2</th>
                                        <th style={{ textAlign: 'center' }} >F3</th>
                                        <th style={{ textAlign: 'center' }} >F4</th>
                                        <th style={{ textAlign: 'center' }} >F5</th>
                                        <th style={{ textAlign: 'center' }} >Score</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style={{ backgroundColor: 'rgb(164, 205, 157, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(164, 205, 157, 100%)' }} >100</td>
                                    </tr>
                                    <tr style={{ backgroundColor: 'rgb(164, 205, 157, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(164, 205, 157, 100%)' }} >100</td>
                                    </tr>
                                    <tr style={{ backgroundColor: 'rgb(164, 205, 157, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(164, 205, 157, 100%)' }} >100</td>
                                    </tr>
                                    <tr style={{ backgroundColor: 'rgb(164, 205, 157, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(164, 205, 157, 100%)' }} >100</td>
                                    </tr>
                                    <tr style={{ backgroundColor: 'rgb(255, 225, 140, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(255, 225, 140, 100%)' }} >100</td>
                                    </tr>
                                    <tr style={{ backgroundColor: 'rgb(255, 225, 140, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(255, 225, 140, 100%)' }} >100</td>
                                    </tr>
                                    <tr style={{ backgroundColor: 'rgb(255, 202, 145, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(255, 202, 145, 100%)' }} >100</td>
                                    </tr>
                                    <tr style={{ backgroundColor: 'rgb(255, 136, 128, 65%)' }}>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center' }} >14</td>
                                        <td style={{ textAlign: 'center', backgroundColor: 'rgb(255, 136, 128, 100%)' }} >100</td>
                                    </tr>

                                </tbody>
                            </table>
                            <div className="pagination-wrapper">
                                <div className="space"></div>
                                <div className="pagination">
                                    <Pagination
                                        total={totalPages}
                                        current={currentPage}
                                        onPageChange={page => handlePageChange(page)}
                                    />
                                </div>
                                <div className="entries-content">
                                    <span>Showing 1 to 10 of 100 entries</span>
                                </div>
                            </div>
                            <div className='madical-details-submit-btn'>
                                <Link>Submit</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default MedicalIndicationDetals;
