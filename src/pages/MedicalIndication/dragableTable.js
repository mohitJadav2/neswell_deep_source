import React, { useEffect, useState, useRef } from 'react'
import styled from 'styled-components'
import { useTable } from 'react-table'
import { DndProvider, useDrag, useDrop } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import update from 'immutability-helper'
import Layout from '../../Layout';



import makeData from './makeData'
import { MdDirectionsRun, MdDragHandle, MdInvertColors } from 'react-icons/md'
import { IoColorFillSharp } from 'react-icons/io'
import { ColorPicker, useColor } from 'react-color-palette'
import MedicalIndicationApi from '../../services/MedicalIndicationApi'
import { useHistory } from 'react-router-dom'


const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`

const Table = ({ columns, data }) => {
    const [records, setRecords] = React.useState(data)
    const [recordData, setRecordData] = React.useState(true)
    const getRowId = React.useCallback(row => {
        return row.id
    }, [])
    useEffect(() => {
        MedicalIndicationApi.MedicalIndicationTableData(
            localStorage.getItem("neswell_access_token"),
        ).then((res) => {
            setRecords(res?.data);

        });
    }, []);
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable({
        data: records,
        columns,
        getRowId,
    })

    const moveRow = (dragIndex, hoverIndex) => {
        const dragRecord = records[dragIndex]
        setRecords(
            update(records, {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragRecord],
                ],
            })
        )
        setRecordData(false)
    }
    const [loader, setLoader] = useState(false);
    const [apiError, setApiError] = useState(undefined);
    let history = useHistory();
    const [color, setColor] = useColor("");
    const handleSubmit = async () => {

        let updatedRecord11 = await records?.map((item, idx) => ({ ...item, index: idx + 1 }))
        await MedicalIndicationApi.AddMedicalIndicationTableData(localStorage.getItem("neswell_access_token"), updatedRecord11).then(
            (json) => {
                if (json?.header?.status === 200 || json?.header?.status === 201) {

                    history.push("/table")
                    setLoader(false)
                } else {
                    setApiError(json?.data.error)
                    setLoader(false)
                }
            }
        )
    }
    const handleOrganize = async (item) => {
        let sortArray = []
        let updatedRecordsRed = records?.filter((item => item?.color == '#FF8880'))
        let updatedRecordsYellow = records?.filter((item => item?.color == '#FFCA91'))
        let updatedRecordsOrange = records?.filter((item => item?.color == '#FFE18C'))
        let updatedRecordsGreen = records?.filter((item => item?.color == '#A4CD9D'))
        let updatedRecordsBlank = records?.filter((item => item?.color == ''))

        updatedRecordsGreen.map((item) => (sortArray.push(item)))
        updatedRecordsOrange.map((item) => (sortArray.push(item)))
        updatedRecordsYellow.map((item) => (sortArray.push(item)))
        updatedRecordsRed.map((item) => (sortArray.push(item)))
        updatedRecordsBlank.map((item) => (sortArray.push(item)))
        setRecords(sortArray)
    }
    const handleSetColorInData = (item, id) => {

        let updatedRecords = records?.map((item1) => (item1?.id == id ? { ...item1, color: item } : item1))
        setRecords(updatedRecords)

    }
    const handleSetInputData = (item, id) => {

        let updatedInputData = records?.map((item2) => (item2?.id == id ? { ...item2, score: item } : item2))

        setRecords(updatedInputData)
    }
    return (
        <Layout>
            <div className='dragable-table-wrapper'>
                <div className='medical-indication-details-view-header'>
                    <div className='medical-indication-view-wrapper'>
                        <div className='name-left-column'>
                            <p>Medical Indication: <strong>Indication Name #02</strong></p>
                        </div>
                        <div className='color-right-column'>
                            <p>Task Type: <strong>Score</strong></p>
                            <ul>
                                <li> <span style={{ backgroundColor: '#FF8880', }} className='status-color'></span> 0-39 </li>
                                <li> <span style={{ backgroundColor: '#FFCA91', }} className='status-color'></span> 40-59 </li>
                                <li> <span style={{ backgroundColor: '#FFE18C', }} className='status-color'></span> 60-79 </li>
                                <li> <span style={{ backgroundColor: '#A4CD9D', }} className='status-color'></span> 80-100 </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <DndProvider
                    backend={HTML5Backend}
                >
                    <table className='dragable-table' {...getTableProps()} style={{ border: '1px solid #EBEBEB' }}>
                        <thead style={{ border: '1px solid #FFFFFF' }}>
                            {headerGroups.map(headerGroup => (
                                <tr {...headerGroup.getHeaderGroupProps()} style={{ border: '1px solid #FFFFFF', boxShadow: ' 0px 1px 1px #1C6FE929' }}>
                                    {headerGroup.headers.map(column => (
                                        <th style={{ border: '1px solid #F8F8F8', color: '#757575' }} {...column.getHeaderProps()}>{column.render('Header')}</th>
                                    ))}
                                    <th style={{ border: '1px solid #FFFFFF', boxShadow: ' 0px 1px 1px #1C6FE929', color: '#757575' }}>Score</th>
                                </tr>
                            ))}


                        </thead>
                        <tbody {...getTableBodyProps()} style={{ border: '1px solid #F8F8F8' }}>
                            {rows.map(
                                (row, index) =>
                                    prepareRow(row) || (
                                        <Row
                                            index={index}
                                            row={row}
                                            moveRow={moveRow}
                                            records={(item, id) => handleSetColorInData(item, id)}
                                            textInput={(item, id) => handleSetInputData(item, id)}
                                            {...row.getRowProps()}
                                        />
                                    )

                            )}
                        </tbody>
                    </table>
                </DndProvider>
            </div>
            <div style={{ display: 'flex' }}>
                <div onClick={() => handleOrganize()} className="form-submit-btn">
                    <button className="primary-btn">Organize</button>
                </div>
                <div onClick={() => handleSubmit()} className="form-submit-btn">
                    <button className="primary-btn">Submit</button>
                </div>
            </div>
        </Layout>
    )
}

const DND_ITEM_TYPE = 'row'

const Row = ({ row, index, moveRow, records, textInput }) => {
    const dropRef = React.useRef(null)
    const dragRef = React.useRef(null)

    const [, drop] = useDrop({
        accept: DND_ITEM_TYPE,
        hover(item, monitor) {
            if (!dropRef.current) {
                return
            }
            const dragIndex = item.index
            const hoverIndex = index
            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return
            }
            // Determine rectangle on screen
            const hoverBoundingRect = dropRef.current.getBoundingClientRect()
            // Get vertical middle
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
            // Determine mouse position
            const clientOffset = monitor.getClientOffset()
            // Get pixels to the top
            const hoverClientY = clientOffset.y - hoverBoundingRect.top
            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%
            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return
            }
            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return
            }
            // Time to actually perform the action
            moveRow(dragIndex, hoverIndex)
            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex
        },
    })

    const [{ isDragging }, drag, preview] = useDrag({
        item: { type: DND_ITEM_TYPE, index },
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
    })

    const opacity = isDragging ? 0 : 1

    preview(drop(dropRef))
    drag(dragRef)

    const [displayColorPicker, setDisplayColorPicker] = useState(false);
    const [pricePerkg, setpricePerkg] = useState();
    const [color, setColor] = useColor("");
    const [score, setScore] = useState("");
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef);

    const handleClick = (e) => {
        e.preventDefault();
        setDisplayColorPicker(!displayColorPicker);
    };
    function useOutsideAlerter(ref) {
        useEffect(() => {
            /**
             * Alert if clicked on outside of element
             */
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    setDisplayColorPicker(false);
                }
            }
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }
    const handleColorSet = (color, cell) => {

        records(color, cell.id);
        setColor(color)
    }
    const handleInput = (text, cell) => {
        textInput(text, cell.id)
        setScore(text)
    }
    return (
        <tr ref={dropRef} style={{ opacity, backgroundColor: color, border: '1px solid #F8F8F8', textAlign: 'center' }} >
            {row.cells.map(cell => {
                return <td style={{ border: '1px solid #F8F8F8', color: "#404040" }} {...cell.getCellProps()}>{cell.render('Cell')}</td>
            })}{
                <td className="form-group" style={{ display: 'flex', flexDirection: "row", border: '1px solid #F8F8F8', justifyContent: 'center' }}>
                    <input
                        className="add-user-input"
                        type="text"
                        placeholder="Enter the Score"

                        onChange={(e) => handleInput(e.target.value, row)}
                        style={{ border: "none", }}
                    />

                    <div ref={wrapperRef} >
                        <div onClick={handleClick}>
                            <MdInvertColors />
                        </div>
                        <div>
                            {displayColorPicker && (
                                <ul style={{
                                    backgroundColor: '#EBEBEB',
                                    display: 'flex',
                                    alignItems: 'center',
                                    flexDirection: 'row',
                                    top: 474,
                                    left: 1205,
                                    width: 260,
                                    height: 60,
                                    boxShadow: "0 8 24 #1C6FE942",
                                    borderRadius: 10,
                                    listStyle: 'none',
                                    opacity: 1
                                }}>
                                    <li color={color} onClick={(e) => handleColorSet('#FF8880', row)} style={{
                                        backgroundColor: '#FF8880',
                                        top: 488,
                                        marginRight: 20,
                                        left: 1225,
                                        width: 32,
                                        height: 32,
                                        borderRadius: 5,
                                        opacity: 1
                                    }}></li>
                                    <li color={color} onClick={(e) => handleColorSet('#FFCA91', row)} style={{
                                        backgroundColor: '#FFCA91',
                                        top: 488,
                                        marginRight: 20,
                                        left: 1225,
                                        width: 32,
                                        height: 32,
                                        borderRadius: 5,
                                        opacity: 1
                                    }}> </li>
                                    <li color={color} onClick={(e) => handleColorSet('#FFE18C', row)} style={{
                                        backgroundColor: '#FFE18C',
                                        top: 488,
                                        marginRight: 20,
                                        left: 1225,
                                        width: 32,
                                        height: 32,
                                        borderRadius: 5,
                                        opacity: 1
                                    }}></li>
                                    <li color={color} onClick={(e) => handleColorSet('#A4CD9D', row)} style={{
                                        backgroundColor: '#A4CD9D',
                                        top: 488,
                                        marginRight: 20,
                                        left: 1225,
                                        width: 32,
                                        height: 32,
                                        borderRadius: 5,
                                        opacity: 1
                                    }}></li>
                                    <li color={color} onClick={(e) => handleColorSet('', row)}
                                        style={{
                                            backgroundColor: '#F1F1F1',
                                            top: 488,
                                            marginRight: 20,
                                            left: 1225,
                                            width: 32,
                                            height: 32,
                                            borderRadius: 5,
                                            opacity: 1
                                        }}></li>
                                </ul>)
                            }

                        </div>
                    </div>
                </td>

            }
            <td ref={dragRef} style={{ border: '1px solid #F8F8F8' }}><MdDragHandle /></td>
        </tr>
    )
}

const App = () => {

    const columns = React.useMemo(
        () => [

            {
                Header: 'f1',
                accessor: 'F1',

            },
            {
                Header: 'f2',
                accessor: 'F2',
            },
            {
                Header: 'f3',
                accessor: 'F3',
            },
            {
                Header: 'f4',
                accessor: 'F4',
            },
            {
                Header: 'f5',
                accessor: 'F5',
            },

        ],
        []
    )

    const [data, setData] = useState([]);

    return (
        <Styles>
            <Table columns={columns} data={data} />
        </Styles>
    )
}

export default App
