import namor from 'namor'

const range = len => {
  const arr = []
  for (let i = 0; i < len; i++) {
    arr.push(i)
  }
  return arr
}

const newPerson = id => {
  const statusChance = Math.random()
  return {
    f1: namor.generate({ words: 0, numbers: 0 }),
    f2: namor.generate({ words: 0, numbers: 0 }),
    f3: namor.generate({ words: 0, numbers: 0 }),
    f4: namor.generate({ words: 0, numbers: 0 }),
    f5: namor.generate({ words: 0, numbers: 0 }),
    score: namor.generate({ words: 0, numbers: 0 }),
  }
}

export default function makeData(...lens) {
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth]
    return range(len).map(i => {
      return {
        ...newPerson(i),
        subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined,
      }
    })
  }

  return makeDataLevel()
}