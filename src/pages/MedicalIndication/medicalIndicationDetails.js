import React, { useState, useEffect, useRef } from "react";
import "./medicalIndication.style.scss";
import { BiEdit } from "react-icons/bi";
import { MdContentCopy } from "react-icons/md";
import { AiOutlinePlusCircle, AiOutlineEye, AiOutlineSetting } from "react-icons/ai";
import Layout from "../../Layout";
import { Link, useHistory } from "react-router-dom";
import Pagination from "react-responsive-pagination";
import MedicalIndicationApi from "../../services/MedicalIndicationApi";
import Modal from "../../components/ModalComponent";
import { RiDeleteBin5Line } from "react-icons/ri";
import ReactTooltip from 'react-tooltip';
import Loader from "../../components/loader";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";
import { FiSearch } from "react-icons/fi";

function MedicalIndicationDetals() {
  // const totalPages = 100;
  const [medicalIndiacationData, setMedicalIndiacationData] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [orderType, setOrderType] = useState("asc");
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(undefined);
  const [totalPages, setTotalPages] = useState(undefined);
  const [orderField, setOrderField] = useState("id");
  const [searchText, setSearchText] = useState("");
  const [loader, setLoader] = useState(false);
  const [openDropDown, setOpenDropDown] = useState(undefined);
  const [toggleDropDown, setToggleDropDown] = useState(false);
  const [searchError, setSearchError] = useState(false);
  let history = useHistory()
  const perPageValue = [10, 25, 50, 100];

  function handlePageChange(page) {
    setLoader(true);
    setSearchText(searchText);
    setCurrentPage(page);
    MedicalIndicationApi.MedicalIndicationData(
      localStorage.getItem("neswell_access_token"),
      page,
      perPage
    ).then((json) => {
      if (!json.data.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setMedicalIndiacationData(json?.data?.results);
        setCurrentPage(page);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }

  useEffect(() => {
    setLoader(true)
    MedicalIndicationApi.MedicalIndicationData(
      localStorage.getItem("neswell_access_token"),
      page,
      perPage
    ).then((res) => {
      setMedicalIndiacationData(res?.data?.results);
      let totalPages = res?.data?.count / perPage;
      setTotalPages(Math.ceil(totalPages));
      setTotalCount(res?.data?.count);
      setLoader(false)
    });
  }, []);

  const handeleDeleteClicked = () => {
    MedicalIndicationApi.MedicalIndicationDelete(
      localStorage.getItem("neswell_access_token"),
      deleteUserData?.id
    ).then((json) => {
      if (!json?.data?.error) {
        MedicalIndicationApi.MedicalIndicationData(
          localStorage.getItem("neswell_access_token"),
          page,
          perPage
        ).then((res) => {
          setMedicalIndiacationData(res?.data?.results);
          let totalPages = res?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(res?.data?.count);
          setModalOpen(false)
        });
      } else {
        setLoader(false);
      }
    });
  };

  const handleCopyClick = async(item) => {
    setLoader(true)
    // await history?.push(`/create-bio/${item.id}`)
    history.push({
        pathname: `/create-medical-indication/${item.id}`,
    })}
   /**
     * This function manage popof setting for every user 
    */
  
    const handleSettingClick = async (item) => {
      console.log("item-------><",item)
      openDropDown == item?.id
        ? setOpenDropDown(undefined)
        : setOpenDropDown(item?.id);
    };

     /**
     * This function manage click outside of the popup box 
    */
      function useOutsideAlerter(ref) {
        useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
            setOpenDropDown(undefined);
            }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
        }, [ref]);
    }
     /**this ref is for the when user clicked outside the fucntion */
     const wrapperRef = useRef(null);
     useOutsideAlerter(wrapperRef);

     /**
     * This function manage search feature 
     * This function passes user's enter value to API and get positive and error response
    */
  const handleSearch = (searchText) => {
    setSearchText(searchText);
    setCurrentPage(page );
    MedicalIndicationApi.SearchMedicalIndicationData(
      orderType,
      orderField,
      localStorage.getItem("neswell_access_token"),
      searchText,
      currentPage,
      perPage
    ).then((json) => {
      if (!json?.data?.error) {
        console.log(json?.data)
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setMedicalIndiacationData(json?.data?.results);
        setSearchText(searchText)
        if(json?.data?.length == 0){
          setSearchError("No records found")
        }else{
          setSearchError(undefined)
        }
        
      }
    });
  };
    
     /**
     * This function manages record display drop down 
     * When user change value of drop down at that time  this function call API with change value
    */
  const handlePerPageChanges = async (perPageNuumber) => {
    setToggleDropDown(false);
    setPerPage(perPageNuumber);
    // if (searchText) {
    //   await MedicalIndicationApi.MedicalIndicationData(
    //     orderType,
    //     orderField,
    //     localStorage.getItem("neswell_access_token"),
    //     String,
    //     1
    //   ).then(async (json) => {
    //     if (!json?.data?.error) {
    //       let totalPages = json?.data?.count / perPageNuumber;
    //       setTotalPages(Math.ceil(totalPages));
    //       setTotalCount(json?.data?.count);
    //       setMedicalIndiacationData(json?.data?.results);
    //     } 
    //   });
    // } else {
      await MedicalIndicationApi.MedicalIndicationData(
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((res) => {
        setMedicalIndiacationData(res?.data?.results);
        let totalPages = res?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setTotalCount(res?.data?.count);
        setLoader(false)
      });
    // }
  };
  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => {
          deleteModal ? handeleDeleteClicked() : setModalOpen(false);
        }}
        title={"Are you sure?"}
        subTitle={
          deleteModal
            ? "Do you want to delete this record?"
            : "We have emailed user password reset link!"
        }
        delete={deleteModal}
      />
      <div className="add-users-details-wrapper">
        {/* <div className="users-title">
          <h2>Data Selection </h2>
        </div> */}
        <div className="medical-indication-details-wrapper">
          {/* <div className="task-radio-btn-group">
            <p>Select a task type</p>
            <ul>
              <li>
                <div class="radio">
                  <input id="View" name="radioBtn" type="radio" />
                  <label for="View" class="radio-label">
                    View
                  </label>
                </div>
              </li>
              <li>
                <div class="radio">
                  <input id="Score" name="radioBtn" type="radio" />
                  <label for="Score" class="radio-label">
                    Score
                  </label>
                </div>
              </li>
              <li>
                <div class="radio">
                  <input id="Predict" name="radioBtn" type="radio" />
                  <label for="Predict" class="radio-label">
                    Predict
                  </label>
                </div>
              </li>
            </ul> */}
          {/* </div */}
          <div className="medical-indication-table users-details-inner-wrapper">
            <div className="user-search">
              <div className="search">
                <button className="search-icon">
                  {" "}
                  <FiSearch />{" "}
                </button>
                <input
                  onChange={(e) => handleSearch(e.target.value)}
                  type="search"
                  placeholder="Search"
                  className="search-input"
                />
              </div>
              <div className="medical-indication-table-title">
                {/* <p>Select Medical Indication</p> */}
                {/* <span>Or</span> */}
                <Link to="/create-medical-indication">
                  {" "}
                  <AiOutlinePlusCircle /> Create Medical Indication
                </Link>
              </div>
            </div>
            <div className="user-details-table details-table">
              <table>
                <thead>
                  <tr>
                    {/* <th ></th> */}
                    <th >Sr.</th>
                    <th >Names</th>
                    <th style={{textAlign: "left" }}>
                      Created at
                    </th>
                    <th style={{textAlign: "left" }}>
                      Score
                    </th>
                    <th style={{textAlign: "center" }}>
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {/* <tr>
                                        <td>
                                            <div class="radio">
                                                <input id="radio-1" name="radio" type="radio" />
                                            </div>
                                        </td>
                                        <td>#001</td>
                                        <td>Nov-Bio-Test00125</td>
                                        <td style={{ textAlign: 'left' }} >15</td>
                                        <td style={{ textAlign: 'center' }} className='text'> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><BiEdit /></span> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><MdContentCopy /></span> <span className="blue-color-eyes" style={{ fontSize: 20, }}><AiOutlineEye /></span> </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="radio">
                                                <input id="radio-1" name="radio" type="radio" />
                                            </div>
                                        </td>
                                        <td>#001</td>
                                        <td>Nov-Bio-Test00125</td>
                                        <td style={{ textAlign: 'left' }} >15</td>
                                        <td style={{ textAlign: 'center' }} className='text'> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><BiEdit /></span> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><MdContentCopy /></span> <span className="blue-color-eyes" style={{ fontSize: 20, }}><AiOutlineEye /></span> </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="radio">
                                                <input id="radio-1" name="radio" type="radio" />
                                            </div>
                                        </td>
                                        <td>#001</td>
                                        <td>Nov-Bio-Test00125</td>
                                        <td style={{ textAlign: 'left' }} >15</td>
                                        <td style={{ textAlign: 'center' }} className='text'> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><BiEdit /></span> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><MdContentCopy /></span> <span className="blue-color-eyes" style={{ fontSize: 20, }}><AiOutlineEye /></span> </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="radio">
                                                <input id="radio-1" name="radio" type="radio" />
                                            </div>
                                        </td>
                                        <td>#001</td>
                                        <td>Nov-Bio-Test00125</td>
                                        <td style={{ textAlign: 'left' }} >15</td>
                                        <td style={{ textAlign: 'center' }} className='text'> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><BiEdit /></span> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><MdContentCopy /></span> <span className="blue-color-eyes" style={{ fontSize: 20, }}><AiOutlineEye /></span> </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="radio">
                                                <input id="radio-1" name="radio" type="radio" />
                                            </div>
                                        </td>
                                        <td>#001</td>
                                        <td>Nov-Bio-Test00125</td>
                                        <td style={{ textAlign: 'left' }} >15</td>
                                        <td style={{ textAlign: 'center' }} className='text'> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><BiEdit /></span> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><MdContentCopy /></span> <span className="blue-color-eyes" style={{ fontSize: 20, }}><AiOutlineEye /></span> </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="radio">
                                                <input id="radio-1" name="radio" type="radio" />
                                            </div>
                                        </td>
                                        <td>#001</td>
                                        <td>Nov-Bio-Test00125</td>
                                        <td style={{ textAlign: 'left' }} >15</td>
                                        <td style={{ textAlign: 'center' }} className='text'> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><BiEdit /></span> <span className="blue-color-eyes" style={{ marginRight: 20, fontSize: 20, }}><MdContentCopy /></span> <span className="blue-color-eyes" style={{ fontSize: 20, }}><AiOutlineEye /></span> </td>
                                    </tr> */}
                  {medicalIndiacationData?.map((item, idx) => {
                    return (
                      <tr>
                        {/* <td>
                          <div class="radio">
                            <input id="radio-1" name="radio" type="radio" />
                          </div>
                        </td> */}
                        <td>{`${item?.id}`}</td>
                        <td>{`${item?.name}`}</td>
                        <td
                          style={{ textAlign: "left" }}
                        >{`${item?.created}`}</td>
                        <td
                          style={{ textAlign: "left" }}
                        >false</td>
                        {/* <td style={{ textAlign: "center" }} className="text">
                          {" "}
                          <span
                            data-tip="Edit"
                            className="blue-color-eyes"
                            style={{ marginRight: 20, fontSize: 20, cursor: 'pointer' }}
                          >
                            <BiEdit />
                            <ReactTooltip backgroundColor="#1C6FE9"/>
                          </span>{" "}
                          <span
                            data-tip="Duplicate"
                            className="blue-color-eyes"
                            style={{ marginRight: 20, fontSize: 20, cursor: 'pointer' }}
                          >
                            <MdContentCopy onClick={() => handleCopyClick(item)}/>
                            <ReactTooltip backgroundColor="#1C6FE9"/>
                          </span>{" "}
                          <span
                            data-tip="Delete"
                            className="blue-color-eyes"
                            style={{ fontSize: 20, cursor: 'pointer' }}
                          >
                            <RiDeleteBin5Line
                              onClick={() => {
                                setDeleteModal(true);
                                setModalOpen(true);
                                setDeleteUserData(item);
                              }}
                            />
                             <ReactTooltip backgroundColor="#1C6FE9"/>
                          </span>{" "}
                        </td> */}
                        <td>
                            {/* {console.log("item------->",item?.id), openDropDown} */}
                          <span  onClick={() => handleSettingClick(item)} className="blue-color-eyes">
                            <AiOutlineSetting style={{ color: "#404040" }} />
                            {openDropDown == item?.id && 
                              <div className="user-setting-popup"
                              ref={wrapperRef}  
                              >
                              <p>View</p>
                              <p>Score</p>
                              <p>Predict</p>
                              <p>Duplicate</p>
                              <p>Remove</p>
                            </div>}
                          </span>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              <p style={{textAlign:"center"}}>{searchError}</p>
              <div className="pagination-wrapper">
                <div className="entries-content">
                  <span>{`Showing 1 to ${medicalIndiacationData?.length} of ${totalCount} entries`}</span>
                </div>
                <div className="pagination">
                  <Pagination
                    total={totalPages}
                    current={currentPage}
                    onPageChange={(page) => handlePageChange(page)}
                  />
                </div>
                <div className="per-pagination-wrapper">
                  <div
                    onClick={() => setToggleDropDown(!toggleDropDown)}
                    className={
                      toggleDropDown ? "selected-page open" : "selected-page "
                    }
                  >
                    <p>{perPage}</p>
                    <div className="up-down-arrow">
                      <span className="down-arrow">
                        <BsChevronDown />
                      </span>
                      <span className="up-arrow">
                        <BsChevronUp />
                      </span>
                    </div>
                  </div>
                  {toggleDropDown && (
                    <ul className="per-page-dropdown">
                      {perPageValue?.map((item) => (
                        <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
              {/* <div className="madical-details-submit-btn">
                <Link>Submit</Link>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default MedicalIndicationDetals;
