import React, { useState, useEffect, useRef } from "react";
import Layout from "../../Layout";
// import "./bioTest.style.scss";
import { FiSearch } from "react-icons/fi";
import Pagination from "react-responsive-pagination";
import BioTestApi from "../../services/BioTestApi";
import { saveFatherBioTests } from "../../Actions/HomeAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import Loader from "../../components/loader";
import { BsChevronDown, BsChevronUp, BsPlusCircle } from "react-icons/bs";
import { AiOutlineSetting } from "react-icons/ai";

function SubBioTestList(props) {
    const [totalPages, setTotalPages] = useState(undefined);
    const [currentPage, setCurrentPage] = useState(1);
    const [subBioTestes, setSubBioTestes] = useState(undefined);
    const [totalCount, setTotalCount] = useState(undefined);
    const [loader, setLoader] = useState(false);
    let history = useHistory();
    const [perPage, setPerPage] = useState(10);
    const [toggleDropDown, setToggleDropDown] = useState(false);
    const [searchText, setSearchText] = useState("");
    const [error, setError] = useState(false)
    const [openDropDown, setOpenDropDown] = useState(undefined);    
    const perPageValue = [10, 25, 50, 100];


    /** 
     * this style is use for the selection fild
    */
    const customSelectStyles = {
        option: (provided, state) => ({
            ...provided,
            color: state.isSelected ? "#fff" : "#404040",
            padding: 10,
            borderRadius: 10,
            width: "98%",
            margin: "10px auto",
        }),
        control: (styles) => ({ ...styles, border: 'solid 1px #E5E5E5', backgroundColor: 'white', maxHeight: 46, minHeight: 46, fontSize: 14, borderRadius: 10, color: '#929292', fontWeight: '600' }),
    };



    /** 
     * This useEffect() manage bioTest records
     */
    useEffect(() => {
        setLoader(true);
        BioTestApi.getAllSubBioTestList(
            localStorage.getItem("neswell_access_token"),
            currentPage,
            perPage
        ).then((json) => {
            if (!json?.data?.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setTotalCount(json?.data?.count);
                setSubBioTestes(json?.data?.results);
                setLoader(false);
            } else {
                setLoader(false);
            }
        });
    }, []);

    /**
     * This function manage popof setting for every user 
    */
  
     const handleSettingClick = async (item) => {
        openDropDown == item?.id
          ? setOpenDropDown(undefined)
          : setOpenDropDown(item?.id);
      };

    /**
     * This function manage click outside of the popup box 
    */
     function useOutsideAlerter(ref) {
        useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
            setOpenDropDown(undefined);
            }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
        }, [ref]);
    }

    /**this ref is for the when user clicked outside the fucntion */
     const wrapperRef = useRef(null);
     useOutsideAlerter(wrapperRef);
    /**
      * This function manages pagination with passing page number in API
     */
    function handlePageChange(page) {
        setLoader(true);
        setCurrentPage(page);
        BioTestApi.getAllSubBioTestListPagination(
            localStorage.getItem("neswell_access_token"),
            page,
            perPage
        ).then((json) => {
            if (!json.data.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setSubBioTestes(json?.data?.results);
              
                setLoader(false);
            } else {
                setLoader(false);
            }
        });
    }

    /**
       * This function manage search feature 
       * This function passes user's enter value to API and get positive and error response
    */
    const handleSearch = (value) => {
        setSearchText(value)
        BioTestApi.searchSubBioTestes(
            localStorage.getItem("neswell_access_token"),
            value,
            // perPage
        ).then((json) => {
            if (!json?.data.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setSubBioTestes(json?.data?.results);
                if(json?.data?.results == undefined){
                    setError(true)
                }else{
                    setError(false)
                }
            } else {
                setError(true)
            }
        });
    };

     /**
     * This function manages record display drop down 
     * When user change value of drop down at that time  this function call API with change value
    */
  const handlePerPageChanges = async (perPageNuumber) => {
    setToggleDropDown(false)
    setPerPage(perPageNuumber)
    if (searchText) {
        BioTestApi.searchSubBioTestes(
            localStorage.getItem("neswell_access_token"),
            searchText,
        ).then((json) => {
            if (!json?.data.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setSubBioTestes(json?.data?.results);
                if(json?.data?.results == undefined){
                    setError(true)
                }else{
                    setError(false)
                }
            } else {
                setError(true)
            }
        })
    } else {
      await BioTestApi.getAllSubBioTestListPerPage(
        localStorage.getItem("neswell_access_token"),
        currentPage,
        perPageNuumber
      ).then(
        async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setSubBioTestes(json?.data?.results)
          }
        }
      )
    }
  }
  /** 
     * This fucntion manage to redirect to create bio page for creat a new sub bio test 
    */
   const handleCreatClick = () => {
    // props.saveFatherBioTestNumber(props?.match?.params?.slug)
    history.push({
        pathname: `/create-bio`,
        state: { userDetail: props?.match?.params?.slug }
    })
    }
    const handleViewChildBio = (id) => {
        // props.saveFatherBioTestNumber(props?.match?.params?.slug)
        // history.push(`/view-bio-details/${id}`)
        history.push({
            pathname: `/view-bio-details/${id}`,
            state: { userDetail: `/sub-bio-test-list` }
        })
    }

     /**
      * This function manage Duplicate sub bio test button and user redirect to create sub bio test page
    */
      const handleDuplicateChild = (id) => {
        history.push({
        pathname: `/create-bio/${id}`,
        state: { userDetail:props?.match?.params?.slug },
        });
    };

    
    return (
        <Layout>
            {loader && <Loader />}
            <div className="bio-test-list-wrapper">
                <div className="bio-test-title-list-icon">
                    <h2>Sub Bio-Tests ({subBioTestes?.length})</h2>
                    <button onClick={() => handleCreatClick()} className="add-filter primary-btn"> <BsPlusCircle /> Create Sub Bio Test </button>
                </div>
                <div className="bio-test-search">
                    <div className="search">
                        <button className="search-icon">
                            {" "}
                            <FiSearch />{" "}
                        </button>
                        <input
                            onChange={(e) => handleSearch(e.target.value)}
                            type="search"
                            placeholder="Search"
                            className="search-input"
                        />
                    </div>
                </div>
                <div className='list-view-table details-table'>
                    <table>
                        <thead>
                            <tr>
                                <th style={{ width: "40px" }} >Sr.</th>
                                <th>Sub Bio-Tests</th>
                                <th>General Bio-test</th>
                                <th>Created at</th>
                                <th>Created by</th>
                                <th>Scored</th>
                                <th style={{ width: "140px", textAlign: 'left', }}>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {subBioTestes?.map((item, idx) => (

                                <tr >
                                    <td onClick={() => {handleViewChildBio(item.id)}} style={{ cursor: "pointer" }}>{item?.id}</td>
                                    <td onClick={() => {handleViewChildBio(item.id)}} style={{ cursor: "pointer" }}>{item?.name}</td>
                                    <td onClick={() => {handleViewChildBio(item.id)}} style={{ cursor: "pointer" }}>{item?.general_bio_test_id}</td>
                                    <td onClick={() => {handleViewChildBio(item.id)}} style={{ cursor: "pointer" }}>{item?.created}</td>
                                    <td onClick={() => {handleViewChildBio(item.id)}} style={{ cursor: "pointer" }}>{item?.user_id}</td>
                                    <td onClick={() => {handleViewChildBio(item.id)}} style={{ cursor: "pointer" }}>False</td>
                                    <td>
                                    <span  onClick={() => handleSettingClick(item)} className="blue-color-eyes">
                                        <AiOutlineSetting style={{ color: "#404040" }} />
                                        {openDropDown == item?.id && 
                                        <div className="user-setting-popup"
                                        ref={wrapperRef}  
                                        >
                                        <p onClick={() => {handleViewChildBio(item.id)}}>View</p>
                                        <p>Score</p>
                                        <p>Predict</p>
                                        <p onClick={() => { handleDuplicateChild(item.id) }}>Duplicate</p>
                                        <p>Remove</p>
                                        </div>}
                                    </span>
                                    </td>
                                    {/* <td>
                                        <span className="blue-color-eyes" onClick={() => handleSettingClick(item)}>

                                            <AiOutlineSetting style={{ color: "#404040" }} />
                                            {openDropDown == item?.id && (
                                                <div ref={wrapperRef} className="user-setting-popup">
                                                    <p onClick={() => { handleViewChildBio(item.id) }}>View</p>
                                                    <p>Score</p>
                                                    <p>Predict</p>
                                                    <p onClick={() => { handleDuplicateChild(item.id) }}>Duplicate</p>
                                                    <p onClick={() => {
                                                        setDeleteModal(true)
                                                        setModalOpen(true)
                                                        setDeleteUserData(item)
                                                    }}>
                                                        Remove
                                                    </p>
                                                </div>
                                            )}
                                        </span>
                                    </td> */}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                        {error && <p style={{textAlign:'center'}}>No data available</p>}
                    <div className="pagination-wrapper">
                <div className="entries-content">
                  <span>{`Showing 1 to ${subBioTestes?.length} of ${totalCount} entries`}</span>
                </div>
                <div className="pagination">
                  <Pagination
                    total={totalPages}
                    current={currentPage}
                    onPageChange={(page) => handlePageChange(page)}
                  />
                </div>
                <div className="per-pagination-wrapper">
                  <div
                    onClick={() => setToggleDropDown(!toggleDropDown)}
                    className={
                      toggleDropDown ? "selected-page open" : "selected-page "
                    }
                  >
                    <p>{perPage}</p>
                    <div className="up-down-arrow">
                      <span className="down-arrow">
                        <BsChevronDown />
                      </span>
                      <span className="up-arrow">
                        <BsChevronUp />
                      </span>
                    </div>
                  </div>
                  {toggleDropDown && (
                    <ul className="per-page-dropdown">
                      {perPageValue?.map((item) => (
                        <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
                </div>
            </div>
        </Layout>
    );
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ saveFatherBioTests }, dispatch),
    };
}

export default connect(null, mapDispatchToProps)(SubBioTestList);







