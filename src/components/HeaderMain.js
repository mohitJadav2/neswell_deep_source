import React from "react";
import { FiBox, FiUsers } from "react-icons/fi";
import ExtractIcon from "../assets/images/Layer.svg";
import { BsCardChecklist, BsChevronLeft } from "react-icons/bs";
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import { AiOutlineExperiment } from 'react-icons/ai';
import { GrCalculator } from 'react-icons/gr';

const Header = (props) => {
  const url = window.location.href;
  let history = useHistory();
  return (
    <header>
        {/* 
          *this condition is for the chang the name of the header as per the current page 
        */}
      {url.includes("sub-bio-test-list") ? (
        <div>
          <h1>
            <BsCardChecklist />
            Sub Bio-Tests Management{" "}
          </h1>
          <div className="back-arrow">
            {(url.includes("view-extract") || 
              url.includes("create-extract"))
              && 
            <div 
              style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
              onClick={() => history.goBack()}
            >
              <BsChevronLeft/> <p>Back</p>
            </div>}
          </div>
        </div>
      ):url.includes("bio-test-list") ||
      url.includes("bio-test-view") ||
      url.includes("view-bio-details") ||
      url.includes("create-bio") ? (
        <div>
          <h1>
            {" "}
            <BsCardChecklist />Bio-Tests Management{" "}
          </h1>
          <div className="back-arrow">
            {(url.includes("bio-test-view") || 
              url.includes("view-bio-details") || 
              url.includes("create-bio")
              )
              && 
            <div 
              style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
              onClick={() => history.goBack()}
            >
              <BsChevronLeft/> <p>Back</p>
            </div>}
          </div>
        </div>
      ) : url.includes("users-extract") ||
        url.includes("view-extract") ||
        url.includes("create-extract") ? (
        <div>
          <h1>
            <img src={ExtractIcon} alt="" style={{ marginRight: 10 }} />
            Extracts Management
          </h1>
          <div className="back-arrow">
            {(url.includes("view-extract") || 
              url.includes("create-extract"))
              && 
            <div 
              style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
              onClick={() => history.goBack()}
            >
              <BsChevronLeft/> <p>Back</p>
            </div>}
          </div>
        </div>
      ) : url.includes("users") ||
      url.includes("view-extract") ||
      url.includes("create-extract") ? (
      <div>
        <h1>
          <FiUsers />
          Users Management
        </h1>
        <div className="back-arrow">
          {(url.includes("add-users") || 
            url.includes("users-profile"))
            && 
          <div 
            style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
            onClick={() => history.goBack()}
          >
            <BsChevronLeft/> <p>Back</p>
          </div>}
        </div>
      </div>
    ) : url.includes("users") ||
      url.includes("view-extract") ||
      url.includes("create-extract") ? (
      <div>
        <h1>
          <FiUsers />
          Users Management
        </h1>
        <div className="back-arrow">
          {(url.includes("view-extract") || 
            url.includes("create-extract"))
            && 
          <div 
            style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
            onClick={() => history.goBack()}
          >
            <BsChevronLeft/> <p>Back</p>
          </div>}
        </div>
      </div>
    ) : url.includes("medical-indication-details") ||
    url.includes("create-medical-indication") ? (
    <div>
      <h1>
        <GrCalculator />
        Medical Indication Management
      </h1>
      <div className="back-arrow">
        {(url.includes("view-extract") || 
          url.includes("create-extract")) 
          && 
        <div 
          style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
          onClick={() => history.goBack()}
        >
          <BsChevronLeft/> <p>Back</p>
        </div>}
      </div>
    </div>
  ) : url.includes("suppliers-data") ||
  url.includes("create-suppliers") || url.includes("view-suppliers") ? (
  <div>
    <h1>
      <FiBox />
      Suppliers Management
    </h1>
    <div className="back-arrow">
      {(url.includes("view-extract") || 
        url.includes("create-extract"))
        && 
      <div 
        style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
        onClick={() => history.goBack()}
      >
        <BsChevronLeft/> <p>Back</p>
      </div>}
    </div>
  </div>
) : (
        <div>
          <h1>
          <AiOutlineExperiment />
            Experiments Management
          </h1>
          <div className="back-arrow">
            {(url.includes("add-users") || 
                url.includes("users-profile") || 
                url.includes("upload-experiment") ||
                url.includes("view-experiment"))
                && 
              <div 
                style={{cursor: "pointer", display: "flex", alignItems: 'center'}}
                onClick={() => history.goBack()}
              >
                <BsChevronLeft/> <p>Back</p>
              </div>}
          </div>
        </div>
      )}
    </header>
  );
};

const mapStateToProps = (state) => ({
  saveFatherNumber: state.homeReducer.saveFatherNumber
});

export default connect(mapStateToProps, null)(Header);
