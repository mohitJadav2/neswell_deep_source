import React, { useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import Modal from "react-modal";
import { AiOutlineCloseCircle, AiOutlineCheckCircle } from "react-icons/ai";
import "./modal.style.scss";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 30,
    borderRadius: 10,
    border: 'none',
    width: 400,
    boxShadow: '0 6px 16px rgba(#000, 0.16)'
  },
};
/**
 * This function manage pop of application like delete, error
 */
const ModalComponent = (props) => {
  let subtitle;

  return (
    <div>
      <Modal
        isOpen={props.open}
        style={customStyles}
        contentLabel="Example Modal"
        overlayClassName={"react-modal-body"}
      >
        <div className="user-profile-popup">
          {props?.delete ? (
            <span className="close-icon">
              <AiOutlineCloseCircle />
            </span>
          ) : (
            <span className="checked-icon">
              {" "}
              <AiOutlineCheckCircle />
            </span>
          )}
          {props?.delete && (
            <h2 ref={(_subtitle) => (subtitle = _subtitle)}>{props?.title}</h2>
          )}
          <p>{props?.subTitle}</p>
          <div className={props?.delete ? "popup-footer" : "popup-footer create-popup"}>
            {!props?.fromError && props?.delete && (
              <button className="popup-btn" onClick={() => props?.DeleteUsersRecord()}>Delete</button>
            )}
            {props?.delete ? (
              <button className="popup-btn" onClick={() => props?.closeModal()}>Close</button>
            ) : (
              <button className="popup-btn" onClick={() => props?.DeleteUsersRecord()}>Ok</button>
            )}
          </div>
          
        </div>
      </Modal>
    </div>
  );
};

export default ModalComponent;
