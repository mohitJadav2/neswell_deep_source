import React from 'react'
import loaderGif from "../assets/loader.gif";

const loader = () => {
    return (
        <div style={{position: "absolute", top: 0, left: 0, transform: "translate -50% -50%", zIndex: 999, width: "100%", height: "100%", display: 'flex', alignItems: 'center', justifyContent: 'center', backgroundColor: '#00000063'}}>
            <img style={{borderRadius: "50%", maxWidth: '250px',}} src={loaderGif} alt="" />
        </div>
    )
}

export default loader;